/**
 * FlingActivity.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms. 
 */

package com.amazon.whisperplay.example.fling;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer;
import com.amazon.whisperplay.fling.media.controller.DiscoveryController;
import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer.FutureListener;
import com.amazon.whisperplay.fling.media.service.MediaPlayerStatus;
import com.amazon.whisperplay.fling.media.service.MediaPlayerStatus.MediaCondition;
import com.amazon.whisperplay.fling.media.service.MediaPlayerStatus.MediaState;
import com.amazon.whisperplay.fling.media.service.CustomMediaPlayer.PlayerSeekMode;
import com.amazon.whisperplay.fling.media.service.CustomMediaPlayer.StatusListener;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastStatusCodes;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.sample.castcompanionlibrary.cast.VideoCastManager;
import com.google.sample.castcompanionlibrary.cast.callbacks.VideoCastConsumerImpl;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class FlingActivity extends Activity {
    
    //Fling Variables
    private static final long MONITOR_INTERVAL = 1000L;
    private DiscoveryController mController;
    private StatusListener mListener;
    private boolean mIsMute = false;

    //Cast Variables
    private VideoCastManager mCastManager;
    private final Handler mHandler = new Handler();
    private Timer mCurrentPositionTimer;
    private MediaInfo mSelectedMedia;
    private MediaRouter mMediaRouter;
    
    //Shared Variables
    private Status mStatus = new Status();
    private Object mCurrentDevice;
    private List<Object> mDeviceList = new LinkedList<Object>();
    private static final String TAG = "FlingActivity";
    private static String[] URL_LIST = {
            "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4",
            "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_normal.mp4",
            "http://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_1080p_h264.mov",
            "http://www.archive.org/download/OCEANIA-an-independent-film-from-the-San-Francisco-Bay-Area/OCEANIA-Creative-Commons-AVI-XVID.avi",
            "https://archive.org/download/ElephantsDream/ElephantsDream_64kb.m3u", "https://archive.org/download/ElephantsDream/ed_1024.avi",
            "https://archive.org/download/ElephantsDream/ed_hd.mp4", "https://archive.org/download/ElephantsDream/ed_1024_512kb.mp4",
            "https://archive.org/download/ElephantsDream/ed_hd.ogv"
    };
    private static String[] TITLE_LIST = {
            "Big Buck Bunny", "Big Buck Bunny", "Big Buck Bunny", "Oceania", "Elephants Dream", "Elephants Dream", "Elephants Dream", "Elephants Dream", "Elephants Dream"
    };
    
    //Fling Discovery Listener
    private DiscoveryController.IDiscoveryListener mDiscovery = new DiscoveryController.IDiscoveryListener() {

        @Override
        public void playerDiscovered(RemoteMediaPlayer device) {
            if (mDeviceList.contains(device)) {
                mDeviceList.remove(device);
                Log.i(TAG, "Updating Device:" + device.getName());
            } else {
                Log.i(TAG, "Adding Device:" + device.getName());
            }
            mDeviceList.add(device);
            triggerUpdate();
        }

        @Override
        public void playerLost(RemoteMediaPlayer device) {
            if( mDeviceList.contains(device) ) {
                Log.i(TAG, "Removing Device:" + device.getName());
                
                if( device.equals(mCurrentDevice) && mListener != null ) {
                    device.removeStatusListener(mListener);
                    mCurrentDevice = null;
                }
                mDeviceList.remove(device);
                triggerUpdate();
            }
        }

        @Override
        public void discoveryFailure() {
            Log.e(TAG, "Discovery Failure");
        }

    };


    //Cast Discovery Listener
    private MediaRouter.Callback mCastCallback = new MediaRouter.Callback() {

        public void onRouteAdded(android.support.v7.media.MediaRouter router, android.support.v7.media.MediaRouter.RouteInfo route) {
            CastDevice device = CastDevice.getFromBundle(route.getExtras());
            if (mDeviceList.contains(device)) {
                mDeviceList.remove(device);
                Log.i(TAG, "Updating Device:" + device.getFriendlyName());
            } else {
                Log.i(TAG, "Adding Device:" + device.getFriendlyName());
            }
            mDeviceList.add(device);
            triggerUpdate();
        }

        public void onRouteRemoved(android.support.v7.media.MediaRouter router, android.support.v7.media.MediaRouter.RouteInfo route) {
            CastDevice device = CastDevice.getFromBundle(route.getExtras());
            if( mDeviceList.contains(device) ) {
                Log.i(TAG, "Removing Device:" + device.getFriendlyName());

                if( device.equals(mCurrentDevice) && mListener != null ) {
                    mCurrentDevice = null;
                }
                mDeviceList.remove(device);
                triggerUpdate();
            }
        }
    };

    //Shared picker updater
    void triggerUpdate() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final ListView lv = (ListView) findViewById(R.id.listView1);
                ArrayAdapter<RemoteMediaPlayer> ad = (ArrayAdapter<RemoteMediaPlayer>) lv.getAdapter();
                ad.notifyDataSetChanged();
            }

        });
    }

    //Cast callbacks for connected device
    private VideoCastConsumerImpl mCastConsumer = new VideoCastConsumerImpl() {

        @Override
        public void onConnected() {
        }

        @Override
        public void onApplicationConnected(ApplicationMetadata appMetadata,
                                           String sessionId, boolean wasLaunched) {
            try {
            	//load media immediatly after connecting to the device
            	/*A difference between cast and fling is that cast has to connect to a device after discovery.
            	//Fling connects to the device and the app on the remote device before presenting the device ass available to the app on the local device
            	//the CastCompanion Library presents any device that supports cast, and then it is up to the app to try to connect to the app
            	*/
                mCastManager.loadMedia(mSelectedMedia, true, 0);
            } catch (Exception e) {
                handleFailure(e, "Error Loading Media", true);
            }
            restartCurrentPositionTimer();
            setStatusText();
            invalidateOptionsMenu();
        }

        @Override
        public boolean onApplicationConnectionFailed(int errorCode) {
            mStatus.mCastCondition = errorCode;
            setStatusText();
            return true;
        }

        @Override
        public void onApplicationDisconnected(int errorCode) {
            mStatus.mCastCondition = errorCode;
            setStatusText();
            invalidateOptionsMenu();
        }

        @Override
        public void onFailed(int resourceId, int statusCode) {
            mStatus.mCastCondition = statusCode;
            setStatusText();
        }

        @Override
        public void onRemoteMediaPlayerMetadataUpdated() {
            setStatusText();
            invalidateOptionsMenu();
        }

        @Override
        public void onRemoteMediaPlayerStatusUpdated() {
            setStatusText();
            invalidateOptionsMenu();
        }

        @Override
        public void onApplicationStatusChanged(String appStatus) {
            setStatusText();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Fling initialization
        mController = new DiscoveryController(this);

        //Cast initialization
        mCastManager = VideoCastManager.initialize(this, "4F8B3483", this.getClass(), null);
        mCastManager.enableFeatures(
                VideoCastManager.FEATURE_NOTIFICATION |
                        VideoCastManager.FEATURE_LOCKSCREEN |
                        VideoCastManager.FEATURE_WIFI_RECONNECT |
                        VideoCastManager.FEATURE_CAPTIONS_PREFERENCE |
                        VideoCastManager.FEATURE_DEBUGGING);
        //mCastManager.setContext(this);
        mCastManager.addVideoCastConsumer(mCastConsumer);
        mMediaRouter = MediaRouter.getInstance(this);

        //Picker initialization
        setContentView(R.layout.activity_main);
        final ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setAdapter(new ArrayAdapter<Object>(this, android.R.layout.simple_list_item_activated_1, android.R.id.text1, mDeviceList));
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onResume() {
        super.onResume();

        //Fling Status listener initialization
        mListener = new Monitor();

        //Fling Start device discovery
        mController.start("amzn.thin.pl", mDiscovery);

        //Cast Start device discovery
        mMediaRouter = MediaRouter.getInstance(this);
        mMediaRouter.addCallback(mCastManager.getMediaRouteSelector(), mCastCallback, MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);

        //Initialize device list listener to update options menu when selection changes
        final ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int pos, long id) {
                invalidateOptionsMenu();
                Log.i(TAG, "onItemClick, pos=" + pos);
            }
        });

        //Initialize media url list listener to update options menu when selection changes
        final Spinner spin = (Spinner) findViewById(R.id.spinner1);
        spin.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, android.R.id.text1, URL_LIST));
        spin.setOnItemSelectedListener(new OnItemSelectedListener() {

            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                invalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                invalidateOptionsMenu();
            }

        });
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onPause() {
        super.onPause();
        //Fling Stop device discovery
        mController.stop();
        //Cast Stop device discovery
        mMediaRouter.removeCallback(mCastCallback);
    }

    @Override
    public void onDestroy() {
    	//Cast Stop status update task
        stopCurrentPositionTimer();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.options, menu);
        return true;
    }

    /**
     * @{inheritDoc
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	//Cast Update options menu based on cast device status
        if (mCurrentDevice instanceof CastDevice) {
            final Spinner spin = (Spinner) findViewById(R.id.spinner1);
            final ListView lv = (ListView) findViewById(R.id.listView1);
            MenuItem playmi = menu.findItem(R.id.playItem);

            try {

                if ((mCastManager == null || !mCastManager.isRemoteMoviePlaying()) && spin.getSelectedItem() != null) {
                    playmi.setEnabled(true);
                    playmi.setVisible(true);
                } else {
                    playmi.setEnabled(false);
                }

                MenuItem mi = menu.findItem(R.id.pauseItem);
                if (mCastManager != null && mCastManager.isRemoteMoviePlaying()) {
                    mi.setEnabled(true);
                    mi.setVisible(true);
                    playmi.setVisible(false);
                } else {
                    mi.setEnabled(false);
                    mi.setVisible(false);
                }

                mi = menu.findItem(R.id.stopItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());

                mi = menu.findItem(R.id.forewardItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());

                mi = menu.findItem(R.id.backwardItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());

                mi = menu.findItem(R.id.volumeUpItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());

                mi = menu.findItem(R.id.volumeDownItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());

                mi = menu.findItem(R.id.muteItem);
                mi.setEnabled(mCastManager != null && mCastManager.isRemoteMoviePlaying());
            } catch (Exception e) {
                handleFailure(e, "Error Checking Media State", true);
            }
        //Fling update options menu based on Fling player status.  Default options if no device selected
        } else {
            MediaState state = null;
            synchronized (mStatus) {
                state = mStatus.mState;
            }

            final Spinner spin = (Spinner) findViewById(R.id.spinner1);
            final ListView lv = (ListView) findViewById(R.id.listView1);
            MenuItem playmi = menu.findItem(R.id.playItem);

            if ((state == null || state != MediaState.Playing) && lv.getCheckedItemPosition() >= 0 && spin.getSelectedItem() != null) {
                playmi.setEnabled(true);
                playmi.setVisible(true);
            } else {
                playmi.setEnabled(false);
            }

            MenuItem mi = menu.findItem(R.id.pauseItem);
            if (state != null && state == MediaState.Playing) {
                mi.setEnabled(true);
                mi.setVisible(true);
                playmi.setVisible(false);
            } else {
                mi.setEnabled(false);
                mi.setVisible(false);
            }

            mi = menu.findItem(R.id.stopItem);
            mi.setEnabled(state != null && state != MediaState.Finished && state != MediaState.NoSource);

            mi = menu.findItem(R.id.forewardItem);
            mi.setEnabled(state != null && state == MediaState.Playing);

            mi = menu.findItem(R.id.backwardItem);
            mi.setEnabled(state != null && state == MediaState.Playing);

            mi = menu.findItem(R.id.volumeUpItem);
            mi.setEnabled(state != null && state == MediaState.Playing);

            mi = menu.findItem(R.id.volumeDownItem);
            mi.setEnabled(state != null && state == MediaState.Playing);

            mi = menu.findItem(R.id.muteItem);
            mi.setEnabled(state != null && state == MediaState.Playing);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MediaState state = null;
        synchronized (mStatus) {
            state = mStatus.mState;
        }

        //get the selected device
        final Spinner spin = (Spinner) findViewById(R.id.spinner1);
        final ListView lv = (ListView) findViewById(R.id.listView1);
        ArrayAdapter<Object> ad = (ArrayAdapter<Object>) lv.getAdapter();
        Object target = ad.getItem(lv.getCheckedItemPosition());

        if (item.getItemId() == R.id.playItem) {
            if ((target instanceof RemoteMediaPlayer && state == MediaState.Paused )||( target instanceof CastDevice && mStatus.mCastState == MediaStatus.PLAYER_STATE_PAUSED)) {
            	//Play from current position when in pause state
                doPlay();
            } else {

                String name = (String) spin.getSelectedItem();
                String titleMd = "{ title:\"" + TITLE_LIST[spin.getSelectedItemPosition()] + "\"}";
                
                //load media and begin playback
                fling(target, name, titleMd);
            }
            return true;
        } else if (item.getItemId() == R.id.pauseItem) {
            doPause();
            return true;
        } else if (item.getItemId() == R.id.stopItem) {
            doStop();
            return true;
        } else if (item.getItemId() == R.id.forewardItem) {
            doFore();
            return true;
        } else if (item.getItemId() == R.id.backwardItem) {
            doBack();
            return true;
        } else if (item.getItemId() == R.id.volumeUpItem) {
            doVol(true);
            return true;
        } else if (item.getItemId() == R.id.volumeDownItem) {
            doVol(false);
            return true;
        } else if (item.getItemId() == R.id.muteItem) {
            doMute();
            return true;
        }
        return false;
    }

    //Update the text on the middle of the screen with the status of the current connection
    private void setStatusText() {
        runOnUiThread(new Runnable() {
            public void run() {
                TextView tv = (TextView) findViewById(R.id.textView1);
                //Cast
                if (mCurrentDevice instanceof CastDevice) {
                    if (mCastManager == null || mCastManager.getRemoteMediaPlayer() == null) {
                        tv.setText(R.string.no_media);
                        return;
                    }

                    mStatus.mCastState = mCastManager.getPlaybackStatus();
                    mStatus.mCastIdleReason = mCastManager.getIdleReason();
                    synchronized (mStatus) {
                        String condTxt = " ";
                        if (mStatus.mCastCondition == CastStatusCodes.NETWORK_ERROR) {
                            condTxt = getString(R.string.error_channel);
                        } else if (mStatus.mCastCondition == CastStatusCodes.TIMEOUT) {
                            condTxt = getString(R.string.error_timeout);
                        } else if (mStatus.mCastCondition == CastStatusCodes.INTERRUPTED) {
                            condTxt = getString(R.string.error_interrupted);
                        } else if (mStatus.mCastCondition == CastStatusCodes.AUTHENTICATION_FAILED) {
                            condTxt = getString(R.string.error_authentication);
                        } else if (mStatus.mCastCondition == CastStatusCodes.INVALID_REQUEST) {
                            condTxt = getString(R.string.error_request);
                        } else if (mStatus.mCastCondition == CastStatusCodes.CANCELED) {
                            condTxt = getString(R.string.error_canceled);
                        } else if (mStatus.mCastCondition == CastStatusCodes.NOT_ALLOWED) {
                            condTxt = getString(R.string.error_not_allowed);
                        } else if (mStatus.mCastCondition == CastStatusCodes.APPLICATION_NOT_FOUND) {
                            condTxt = getString(R.string.error_app_not_found);
                        } else if (mStatus.mCastCondition == CastStatusCodes.APPLICATION_NOT_RUNNING) {
                            condTxt = getString(R.string.error_app_not_running);
                        } else if (mStatus.mCastCondition == CastStatusCodes.MESSAGE_TOO_LARGE) {
                            condTxt = getString(R.string.error_message_too_large);
                        } else if (mStatus.mCastCondition == CastStatusCodes.MESSAGE_SEND_BUFFER_TOO_FULL) {
                            condTxt = getString(R.string.error_message_send_buffer_too_full);
                        } else if (mStatus.mCastCondition == CastStatusCodes.INTERNAL_ERROR) {
                            condTxt = getString(R.string.error_internal);
                        } else if (mStatus.mCastCondition == CastStatusCodes.UNKNOWN_ERROR) {
                            condTxt = getString(R.string.error_unknown);
                        }

                        long position = 0;
                        try {
                            position = mCastManager.getCurrentMediaPosition();
                        } catch (Exception e) {
                            handleFailure(e, "Error Getting Media Position", true);
                        }
                        switch (mStatus.mCastState) {
                            case MediaStatus.PLAYER_STATE_IDLE:
                                switch(mStatus.mCastIdleReason) {
                                    case MediaStatus.IDLE_REASON_ERROR:
                                        tv.setText(getString(R.string.media_error, condTxt));
                                        break;
                                    case MediaStatus.IDLE_REASON_FINISHED:
                                        if (condTxt.length() > 1) {
                                            tv.setText(getString(R.string.media_done_error, condTxt));
                                        } else {
                                            tv.setText(R.string.media_done);
                                        }
                                        break;
                                    case MediaStatus.IDLE_REASON_CANCELED:
                                        tv.setText(getString(R.string.media_canceled, condTxt));
                                        break;
                                    case MediaStatus.IDLE_REASON_INTERRUPTED:
                                        tv.setText(getString(R.string.media_interrupted, condTxt));
                                        break;
                                    case MediaStatus.IDLE_REASON_NONE:
                                        tv.setText(getString(R.string.media_idle, condTxt));
                                        break;
                                }
                                break;
                            case MediaStatus.PLAYER_STATE_BUFFERING:
                                tv.setText(getString(R.string.media_preping, condTxt));
                                break;
                            case MediaStatus.PLAYER_STATE_PLAYING:
                                tv.setText(getString(R.string.media_playing, String.valueOf(position), condTxt));
                                break;
                            case MediaStatus.PLAYER_STATE_PAUSED:
                                tv.setText(getString(R.string.media_paused, String.valueOf(position), condTxt));
                                break;
                            case MediaStatus.PLAYER_STATE_UNKNOWN:
                                tv.setText(getString(R.string.media_error, (condTxt.length() == 1 ? "Unknown" : condTxt)));
                                break;

                        }
                    }
                //Fling and no device
                } else {
                    if (mCurrentDevice == null || mStatus.mState == null) {
                        tv.setText(R.string.no_media);
                        return;
                    }

                    synchronized (mStatus) {
                        String condTxt = " ";
                        if (mStatus.mCond == MediaCondition.ErrorChannel) {
                            condTxt = getString(R.string.error_channel);
                        } else if (mStatus.mCond == MediaCondition.ErrorContent) {
                            condTxt = getString(R.string.error_content);
                        } else if (mStatus.mCond == MediaCondition.ErrorUnknown) {
                            condTxt = getString(R.string.error_unknown);
                        } else if (mStatus.mCond == MediaCondition.WarningBandwidth) {
                            condTxt = getString(R.string.warn_bandwidth);
                        } else if (mStatus.mCond == MediaCondition.WarningContent) {
                            condTxt = getString(R.string.warn_content);
                        }

                        switch (mStatus.mState) {
                            case NoSource:
                                tv.setText(R.string.no_media);
                                break;
                            case PreparingMedia:
                            case ReadyToPlay:
                                tv.setText(getString(R.string.media_preping, condTxt));
                                break;
                            case Playing:
                                tv.setText(getString(R.string.media_playing, String.valueOf(mStatus.mPosition), condTxt));
                                break;
                            case Paused:
                                tv.setText(getString(R.string.media_paused, String.valueOf(mStatus.mPosition), condTxt));
                                break;
                            case Finished:
                                if (condTxt.length() > 1) {
                                    tv.setText(getString(R.string.media_done_error, condTxt));
                                } else {
                                    tv.setText(R.string.media_done);
                                }
                                break;
                            case Seeking:
                                tv.setText(R.string.media_seeking);
                                break;
                            case Error:
                                tv.setText(getString(R.string.media_error, condTxt));
                                break;
                            default:
                                tv.setText(getString(R.string.media_error, (condTxt.length() == 1 ? "Unknown" : condTxt)));
                                break;

                        }
                    }
                }
            }
        });
    }

    //Generic failure handler to display error to use and log
    private void handleFailure(Throwable throwable, final String msg, final boolean extend ) {
        Log.e(TAG, msg, throwable);
        final String exceptionMessage = throwable.getMessage();
        
        FlingActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(FlingActivity.this, msg+(extend?exceptionMessage:""), Toast.LENGTH_LONG).show();
            }
        });        
    }
    
    //load media and begin playback
    private void fling(final Object target, final String name, final String title) {
        mIsMute = false;
        mStatus.clear();
        //Fling
        if (target instanceof RemoteMediaPlayer) {
            final RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)target;
            //Load media and handle the result through the listener provided
            currentDevice.addStatusListener(mListener).getAsync(new ErrorResultHandler("Cannot set status listener"));
            currentDevice.setPositionUpdateInterval(MONITOR_INTERVAL).getAsync(new ErrorResultHandler("Error attempting set update interval, ignoring", true));
            currentDevice.setMediaSource(name, title, true, false).getAsync(new ErrorResultHandler("Error attempting to Play:", true));
        //Cast
        } else if (target instanceof CastDevice) {
            MediaInfo.Builder builder = new MediaInfo.Builder(name);
            builder.setContentType("video/mp4");
            builder.setStreamType(MediaInfo.STREAM_TYPE_BUFFERED);
            MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
            movieMetadata.putString(MediaMetadata.KEY_TITLE, title);
            builder.setMetadata(movieMetadata);
            mSelectedMedia = builder.build();
            try {
                if (mCurrentDevice != null && mCurrentDevice.equals(target)) {
                	//load media and handle the result through the mCastConsumer listener
                	//onRemoteMediaPlayerMetadataUpdated will be called on success
                	//onFailed will be called when unsuccessful?
                    mCastManager.loadMedia(mSelectedMedia, true, 0);
                } else {
                	//connect to the application and handle the result through the mCastConsumer listener
                	//onApplicationConnected will be called on success (which later calls loadMedia)
                	//onApplicationConnectionFailed will be called when unsuccessful
                    mCastManager.setDevice((CastDevice) target);
                }
            } catch (Exception e) {
                handleFailure(e, "Error attempting to Play:", true);
            }
        }
        mCurrentDevice = target;
        Toast.makeText(this, "Flinging...", Toast.LENGTH_LONG).show();
    }

    private void doPlay() {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                	//play the current media on the device from the current position on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    mCastManager.play();
                } catch (Exception e) {
                    handleFailure(e, "Error attempting to Play:", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                //play the current media on the device from the current position on the device
                //handle the result through the generic listener created that does nothing on success
                //and logs and displays the error when not successful
                currentDevice.play().getAsync(new ErrorResultHandler("Error Playing"));
            }
        }

        Toast.makeText(this, "Playing...", Toast.LENGTH_LONG).show();
    }

    private void doPause() {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                	//pause the current media on the device from the current position on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    mCastManager.pause();
                    stopCurrentPositionTimer();
                } catch (Exception e) {
                    handleFailure(e, "Error Pausing:", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                //pause the current media on the device from the current position on the device
                //handle the result through the generic listener created that does nothing on success
                //and logs and displays the error when not successful
                currentDevice.pause().getAsync(new ErrorResultHandler("Error Pausing"));
            }
        }

        Toast.makeText(this, "Pausing...", Toast.LENGTH_LONG).show();
    }

    private void doStop() {
        if (mCurrentDevice != null) {
            //Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                    //stop the current media on the device from the current position on the device
                    //handle the result through the mCastConsumer listener
                    //??? will be called on success
                    //onFailed will be called when unsuccessful?
                    mCastManager.stop();
                    stopCurrentPositionTimer();
                } catch (Exception e) {
                    handleFailure(e, "Error Stopping:", true);
                }
                //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                final RemoteMediaPlayer currentDevice = (RemoteMediaPlayer) mCurrentDevice;
                //stop the current media on the device from the current position on the device
                //handle the result through the generic listener using the overridden actionSucceeded below
                //the generic listner logs and displays the error when not successful
                currentDevice.stop().getAsync(new ErrorResultHandler("Error Stopping"));
                currentDevice.removeStatusListener(mListener).getAsync(new ErrorResultHandler("Error removing listener"));
                mCurrentDevice = null;

                setStatusText();
                mStatus.clear();
                invalidateOptionsMenu();
            }
        }

        Toast.makeText(this, "Stopping...", Toast.LENGTH_LONG).show();
    }

    private void doFore() {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                    long position = mCastManager.getCurrentMediaPosition();
                    position += 10000;
                    //seek forward 10 seconds and play the current media on the device from the current position on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    mCastManager.seekAndPlay((int)position);
                } catch (Exception e) {
                    handleFailure(e, "Exception while seeking:", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                //seek forward 10 seconds and play the current media on the device from the current position on the device
                //handle the result through the generic listener created that does nothing on success
                //and logs and displays the error when not successful
                currentDevice.seek(PlayerSeekMode.Relative, 10000).getAsync(new ErrorResultHandler("Error Seeking"));
            }
        }

        Toast.makeText(this, "Seeking Forward 10s...", Toast.LENGTH_LONG).show();
    }

    private void doBack() {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                    long position = mCastManager.getCurrentMediaPosition();
                    position -= 10000;
                    //seek back 10 seconds and play the current media on the device from the current position on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    mCastManager.seekAndPlay((int)position);
                } catch (Exception e) {
                    handleFailure(e, "Exception while seeking:", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                //seek back 10 seconds and play the current media on the device from the current position on the device
                //handle the result through the generic listener created that does nothing on success
                //and logs and displays the error when not successful
                currentDevice.seek(PlayerSeekMode.Relative, -10000).getAsync(new ErrorResultHandler("Error Seeking"));
            }
        }

        Toast.makeText(this, "Seeking Back 10s...", Toast.LENGTH_LONG).show();
    }

    private void doVol(final boolean volUp) {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                	//increase or decrease volume by 10% on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    if (volUp) {
                        mCastManager.incrementVolume(0.1);
                    } else {
                        mCastManager.incrementVolume(-0.1);
                    }
                } catch (Exception e) {
                    handleFailure(e, "Error Getting volume:", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                final RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                currentDevice.getVolume().getAsync(new FutureListener<Double>() {
                    @Override
                    public void futureIsNow(Future<Double> result) {
                        try {
                            double vol = result.get();
                            if (volUp) {
                                vol = Math.min(1.0, vol + 0.1);
                            } else {
                                vol = Math.max(0.0, vol - 0.1);
                            }
                            //increase or decrease volume by 10% on the device
                            //handle the result through the generic listener created that does nothing on success
                            //and logs and displays the error when not successful
                            currentDevice.setVolume(vol).getAsync(new ErrorResultHandler("Error Setting volume"));
                        } catch (ExecutionException e) {
                            handleFailure(e.getCause(), "Error Getting volume", false);
                        } catch (Exception e) {
                            handleFailure(e, "Error Getting volume", false);
                        }
                    }
                });
            }
        }

        Toast.makeText(this, (volUp ? "Increasing" : "Decreasing") + " volume...", Toast.LENGTH_LONG).show();
    }

    private void doMute() {
        if (mCurrentDevice != null) {
        	//Cast
            if (mCurrentDevice instanceof CastDevice) {
                try {
                	//toggle mute on the device
                	//handle the result through the mCastConsumer listener
                	//??? will be called on success
                	//onFailed will be called when unsuccessful?
                    if (mCastManager.isMute()) {
                        mCastManager.setMute(false);
                    } else {
                        mCastManager.setMute(true);
                    }
                } catch (Exception e) {
                    handleFailure(e, "Error Muting", true);
                }
            //Fling
            } else if (mCurrentDevice instanceof RemoteMediaPlayer) {
                RemoteMediaPlayer currentDevice = (RemoteMediaPlayer)mCurrentDevice;
                //toggle mute on the device
                //handle the result through the generic listener using the overridden actionSucceeded below
                //logs and displays the error when not successful
                currentDevice.setMute(!mIsMute).getAsync(new FutureListener<Void>() {
                    @Override
                    public void futureIsNow(Future<Void> result) {
                        try {
                            result.get();
                            mIsMute = !mIsMute;
                        } catch (ExecutionException e) {
                            handleFailure(e.getCause(), "Error Muting", false);
                        } catch (Exception e) {
                            handleFailure(e, "Error Muting", false);
                        }
                    }
                });
            }
        }        

        Toast.makeText(this, "Mute...", Toast.LENGTH_LONG).show();
    }

    //Status object to handle monitoring the status updates recieved of the remote device
    private static class Status {
    	//Fling
        public long mPosition;
        public MediaState mState;
        public MediaCondition mCond;
        //Cast
        public int mCastCondition;
        public int mCastIdleReason;
        public int mCastState;

        public synchronized void clear() {
        	//Fling
            mPosition = -1L;
            mState = MediaState.NoSource;
            //Cast
            mCastState = MediaStatus.PLAYER_STATE_IDLE;
            mCastIdleReason = MediaStatus.IDLE_REASON_NONE;
        }
    }

    //Fling listener that receives updates about the state, condition and position of the remote media player
    //Updates the UI when changes are received
    private class Monitor implements StatusListener {

        @SuppressLint("NewApi")
        @Override
        public void onStatusChange(MediaPlayerStatus status, long position) {
            if (mCurrentDevice != null) {
                synchronized (mStatus) {
                    mStatus.mState = status.getState();
                    mStatus.mCond = status.getCondition();
                    mStatus.mPosition = position;
                    Log.d(TAG, "State Change... state="+mStatus.mState);
                }
                invalidateOptionsMenu();
                setStatusText();
            }
        }
    }

    //Cast stops the task that requests status, condition and position updates from the remote media player
    private void stopCurrentPositionTimer() {
        Log.d(TAG, "Stopped TrickPlay Timer");
        if (null != mCurrentPositionTimer) {
            mCurrentPositionTimer.cancel();
        }
    }

    //Cast starts the task that requests status, condition and position updates from the remote media player
    private void restartCurrentPositionTimer() {
        stopCurrentPositionTimer();
        mCurrentPositionTimer = new Timer();
        mCurrentPositionTimer.scheduleAtFixedRate(new UpdateCurrentPositionTask(), 100, 1000);
        Log.d(TAG, "Restarted TrickPlay Timer");
    }

    //Task to requests status, condition and position updates from the remote media player
    //Updates the UI when changes are recieved
    private class UpdateCurrentPositionTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    setStatusText();
                }
            });
        }
    }

    //Fling generic listener to handle displaying and logging errors
    //does nothing when an action is successful
    private class ErrorResultHandler implements FutureListener<Void> {
        private String mMsg;
        private boolean mExtend;

        ErrorResultHandler(String msg) {
            this(msg, false);
        }

        ErrorResultHandler(String msg, boolean extend) {
            mMsg = msg;
            mExtend = extend;
        }

        @Override
        public void futureIsNow(Future<Void> result) {
            try {
                result.get();
            } catch(ExecutionException e) {
                handleFailure(e.getCause(), mMsg, mExtend);
            } catch(Exception e) {
                handleFailure(e, mMsg, mExtend);
            }
        }
    }
}
