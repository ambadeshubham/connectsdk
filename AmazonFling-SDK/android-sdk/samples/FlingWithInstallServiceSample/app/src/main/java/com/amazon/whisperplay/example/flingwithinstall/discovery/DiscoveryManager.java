/**
 * DiscoveryManager.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

package com.amazon.whisperplay.example.flingwithinstall.discovery;

import android.content.Context;
import android.util.Log;

import com.amazon.whisperplay.example.flingwithinstall.device.FireTVDevice;
import com.amazon.whisperplay.fling.media.controller.DiscoveryController;
import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer;
import com.amazon.whisperplay.install.InstallDiscoveryController;
import com.amazon.whisperplay.install.RemoteInstallService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DiscoveryManager {

    // Debugging TAG
    private static final String TAG = DiscoveryManager.class.getName();

    private static DiscoveryManager manager;
    private DiscoveryUpdateCallback callback;

    private static DiscoveryController mediaPlayerDiscoveryController;
    private static InstallDiscoveryController installDiscoveryController;

    private InstallServiceDiscoveryListener installServiceListener = new InstallServiceDiscoveryListener();
    private MediaPlayerDiscoveryListener mediaPlayerListener = new MediaPlayerDiscoveryListener();

    // Lock object for List synchronization
    private final Object mListAvailableLock = new Object();

    // Set the discovered devices from Discovery controller
    private List<RemoteMediaPlayer> mPlayerDeviceList = new LinkedList<>();
    // Set the discovered devices from Discovery controller
    private List<RemoteInstallService> mInstallDeviceList = new LinkedList<>();
    // Map that represents FireTV Devices
    private Map<String, FireTVDevice> mDeviceMap = new HashMap<>();
    // Comparator to sort device list with alphabet device name order
    private FireTVDeviceComp mComparator = new FireTVDeviceComp();

    public static final String CUSTOM_PLAYER_SERVICE_ID = "amzn.fling.sample.customplayer";

    private DiscoveryManager() {}

    public static synchronized DiscoveryManager getDiscoveryManager(Context context) {
        if (manager == null) {
            manager = new DiscoveryManager();
            mediaPlayerDiscoveryController = new DiscoveryController(context);
            installDiscoveryController = new InstallDiscoveryController(context);
        }
        return manager;
    }

    public void start() {
        Log.i(TAG, "DiscoveryManager - start");
        mediaPlayerDiscoveryController.start(CUSTOM_PLAYER_SERVICE_ID, mediaPlayerListener);
        installDiscoveryController.start(installServiceListener);
    }

    public void stop() {
        Log.i(TAG, "DiscoveryManager - stop");
        mDeviceMap.clear();
        mInstallDeviceList.clear();
        mPlayerDeviceList.clear();
        mediaPlayerDiscoveryController.stop();
        installDiscoveryController.stop();
    }

    public void setCallback(DiscoveryUpdateCallback cb) {
        this.callback = cb;
    }

    public ArrayList<FireTVDevice> getDeviceMapAsList() {
        ArrayList<FireTVDevice> list = new ArrayList<>(mDeviceMap.values());
        Collections.sort(list, mComparator);
        return list;
    }

    private void updateDeviceMap() {
        mDeviceMap.clear();
        for (RemoteInstallService r : mInstallDeviceList) {
            mDeviceMap.put(r.getUniqueIdentifier(), new FireTVDevice(r));
        }
        for (RemoteMediaPlayer r : mPlayerDeviceList) {
            if (mDeviceMap.containsKey(r.getUniqueIdentifier())) {
                FireTVDevice copy = mDeviceMap.get(r.getUniqueIdentifier());
                copy.setRemoteMediaPlayer(r);
                mDeviceMap.put(r.getUniqueIdentifier(), copy);
            } else {
                FireTVDevice dummy = new FireTVDevice(null);
                dummy.setRemoteMediaPlayer(r);
                mDeviceMap.put(r.getUniqueIdentifier(), dummy);
            }
        }
    }

    public class MediaPlayerDiscoveryListener implements DiscoveryController.IDiscoveryListener {
        @Override
        public void playerDiscovered(RemoteMediaPlayer remoteMediaPlayer) {
            synchronized (mListAvailableLock) {
                int threadId = android.os.Process.myTid();

                if (mPlayerDeviceList.contains(remoteMediaPlayer)) {
                    mPlayerDeviceList.remove(remoteMediaPlayer);
                    Log.i(TAG, "[" + threadId + "]" + "player(updating): " + remoteMediaPlayer.getName());
                } else {
                    Log.i(TAG, "["+threadId+"]"+"player(adding): " + remoteMediaPlayer.getName());
                }
                mPlayerDeviceList.add(remoteMediaPlayer);
                updateDeviceMap();
                callback.discoveryResultUpdated(getDeviceMapAsList());
            }
        }

        @Override
        public void playerLost(RemoteMediaPlayer remoteMediaPlayer) {
            synchronized (mListAvailableLock) {
                if (mPlayerDeviceList.contains(remoteMediaPlayer)) {
                    int threadId = android.os.Process.myTid();
                    Log.i(TAG, "[" + threadId + "]" + "playerLost(removing): " + remoteMediaPlayer.getName());
                    mPlayerDeviceList.remove(remoteMediaPlayer);
                    updateDeviceMap();
                    callback.discoveryResultUpdated(getDeviceMapAsList());
                    callback.playerLost(remoteMediaPlayer);
                }
            }
        }

        @Override
        public void discoveryFailure() {}
    }

    public class InstallServiceDiscoveryListener implements InstallDiscoveryController.IInstallDiscoveryListener{

        @Override
        public void installServiceDiscovered(RemoteInstallService remoteInstallService) {
            synchronized (mListAvailableLock) {
                int threadId = android.os.Process.myTid();
                if (mInstallDeviceList.contains(remoteInstallService)) {
                    mInstallDeviceList.remove((remoteInstallService));
                    Log.i(TAG, "[" + threadId + "]" + "install(updating): " + remoteInstallService.getName());
                } else {
                    Log.i(TAG, "["+threadId+"]"+"install(adding): " + remoteInstallService.getName());
                }
                mInstallDeviceList.add(remoteInstallService);
                updateDeviceMap();
                callback.discoveryResultUpdated(getDeviceMapAsList());
            }

        }

        @Override
        public void installServiceLost(RemoteInstallService remoteInstallService) {
            synchronized (mListAvailableLock) {
                if (mInstallDeviceList.contains(remoteInstallService)) {
                    int threadId = android.os.Process.myTid();
                    Log.i(TAG, "["+threadId+"]"+"install(removing): " + remoteInstallService.getName());

                    mInstallDeviceList.remove(remoteInstallService);
                    updateDeviceMap();
                    callback.discoveryResultUpdated(getDeviceMapAsList());
                }
            }
        }

        @Override
        public void discoveryFailure() {}
    }

    private static class FireTVDeviceComp implements Comparator<FireTVDevice> {
        @Override
        public int compare(FireTVDevice player1, FireTVDevice player2) {
            return player1.getName().compareTo(player2.getName());
        }
    }

}
