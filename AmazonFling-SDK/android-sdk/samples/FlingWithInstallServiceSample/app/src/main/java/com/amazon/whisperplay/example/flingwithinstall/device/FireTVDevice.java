/**
 * FireTVDevice.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

package com.amazon.whisperplay.example.flingwithinstall.device;

import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer;
import com.amazon.whisperplay.install.RemoteInstallService;

public class FireTVDevice {

    private RemoteInstallService mRemoteInstallDevice;
    private RemoteMediaPlayer mRemoteMediaPlayer;

    private String uuid;
    private String name;

    public FireTVDevice(RemoteInstallService installService) {
        this.mRemoteInstallDevice = installService;
        if (installService != null) {
            this.uuid = installService.getUniqueIdentifier();
            this.name = installService.getName();
        }
    }

    public void setRemoteMediaPlayer(RemoteMediaPlayer player) {
        this.mRemoteMediaPlayer = player;
        this.uuid = player.getUniqueIdentifier();
        this.name = player.getName();
    }

    public RemoteMediaPlayer getRemoteMediaPlayer() {
        return this.mRemoteMediaPlayer;
    }

    public RemoteInstallService getRemoteInstallDevice() {
        return this.mRemoteInstallDevice;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof FireTVDevice
                && getUniqueIdentifier().equals(((FireTVDevice) obj).getUniqueIdentifier());
    }

    @Override
    public int hashCode() {
        return getUniqueIdentifier().hashCode();
    }

    @Override
    public String toString() {
        return this.name + " (" + this.uuid + ")";
    }

    public String getName() {
        return this.name;
    }

    public String getUniqueIdentifier() {
        return this.uuid;
    }

}
