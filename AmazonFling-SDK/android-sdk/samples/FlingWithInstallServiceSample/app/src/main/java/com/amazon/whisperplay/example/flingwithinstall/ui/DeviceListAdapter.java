/**
 * DeviceListAdapter.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

package com.amazon.whisperplay.example.flingwithinstall.ui;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.amazon.whisperplay.example.flingwithinstall.R;
import com.amazon.whisperplay.example.flingwithinstall.device.FireTVDevice;

import java.util.ArrayList;

public class DeviceListAdapter extends BaseAdapter {

    private Context mContext;
    protected ArrayList<FireTVDevice> mDeviceList;

    public DeviceListAdapter(Context context, ArrayList<FireTVDevice> deviceList) {
        mContext = context;
        mDeviceList = deviceList;
    }

    public void setDeviceList(ArrayList<FireTVDevice> deviceList) {
        mDeviceList = deviceList;
    }

    @Override
    public int getCount() {
        return mDeviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDeviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.device_list, parent, false);
        }
        TextView device_name = (TextView) convertView.findViewById(R.id.device_name);
        TextView fling_available = (TextView) convertView.findViewById(R.id.device_available);

        if (mDeviceList.get(position).getRemoteMediaPlayer() != null) {
            fling_available.setText(R.string.firetv_ready);
        } else {
            fling_available.setText(R.string.firetv_install);
        }
        device_name.setText(mDeviceList.get(position).getName());
        return convertView;
    }
}
