/**
 * DiscoveryUpdateCallback.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

package com.amazon.whisperplay.example.flingwithinstall.discovery;

import com.amazon.whisperplay.example.flingwithinstall.device.FireTVDevice;
import com.amazon.whisperplay.fling.media.controller.RemoteMediaPlayer;

import java.util.ArrayList;

public interface DiscoveryUpdateCallback {

    void discoveryResultUpdated(ArrayList<FireTVDevice> list);
    void playerLost(RemoteMediaPlayer player);
}
