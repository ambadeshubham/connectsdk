package com.amazon.whisperplay.example.installservicesample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazon.whisperplay.install.InstallDiscoveryController;
import com.amazon.whisperplay.install.RemoteInstallService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class InstallClientActivity extends AppCompatActivity {

    // Debugging TAG
    private static final String TAG = InstallClientActivity.class.getName();

    // Set selected player from device picker
    private RemoteInstallService mCurrentDevice;

    // Discovery controller that triggers start/stop discovery
    private InstallDiscoveryController mController;

    // Lock object for mDeviceList synchronization
    private Object mDeviceListAvailableLock = new Object();
    // Set the discovered devices from Discovery controller
    private List<RemoteInstallService> mDeviceList = new LinkedList<>();
    // Comparator to sort device list with alphabet device name order
    private RemoteInstallServiceComp mComparator = new RemoteInstallServiceComp();

    // Application menu
    private Menu mMenu;
    // Device picker adapter
    private ArrayAdapter<String> mPickerAdapter;
    // List of device picker items.
    private List<String> mPickerList = new ArrayList<>();

    private EditText mPacakgeNameEditText;
    private EditText mInstallASINEditText;
    private Button mGetPackageVersionButton;
    private Button mInstallByASINButton;


    private InstallDiscoveryController.IInstallDiscoveryListener mDiscoveryListener =
            new InstallDiscoveryController.IInstallDiscoveryListener() {

        @Override
        public void installServiceDiscovered(RemoteInstallService remoteInstallService) {
            synchronized (mDeviceListAvailableLock) {
                int threadId = android.os.Process.myTid();
                if (mDeviceList.contains(remoteInstallService)) {
                    mDeviceList.remove(remoteInstallService);
                    Log.i(TAG, "[" + threadId + "]" + "serviceDiscovered(updating): " + remoteInstallService.getName());
                } else {
                    Log.i(TAG, "["+threadId+"]"+"serviceDiscovered(adding): " + remoteInstallService.getName());
                }
                mDeviceList.add(remoteInstallService);
                triggerUpdate();
            }
        }

        @Override
        public void installServiceLost(RemoteInstallService remoteInstallService) {
            synchronized (mDeviceListAvailableLock) {
                if (mDeviceList.contains(remoteInstallService)) {
                    int threadId = android.os.Process.myTid();
                    Log.i(TAG, "["+threadId+"]"+"serviceDiscovered(removing): " + remoteInstallService.getName());
                    if (remoteInstallService.equals(mCurrentDevice)) {
                        mCurrentDevice = null;
                    }
                    mDeviceList.remove(remoteInstallService);
                    triggerUpdate();
                }
            }
        }

        @Override
        public void discoveryFailure() {
            Log.e(TAG, "Discovery Failure");
        }

        private void triggerUpdate() {
            Collections.sort(mDeviceList, mComparator);
            mPickerList.clear();
            for (RemoteInstallService device : mDeviceList) {
                mPickerList.add(device.getName());
            }
            // It should be run in main thread since it is updating Adapter.
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPickerAdapter.notifyDataSetChanged();
                    // Calling onPrepareOptionsMenu() to update picker icon
                    invalidateOptionsMenu();
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install_client);
        // Create DiscoveryInstallService
        mController = new InstallDiscoveryController(this);
        // Initialize UI resources
        mPacakgeNameEditText = (EditText) findViewById(R.id.editText_package_name);
        mInstallASINEditText = (EditText) findViewById(R.id.editText_ASIN);
        mGetPackageVersionButton = (Button) findViewById(R.id.button_get_package_version);
        mInstallByASINButton = (Button) findViewById(R.id.button_install_by_asin);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Start Discovery InstallService
        Log.i(TAG, "onResume - start Discovery");
        mController.start(mDiscoveryListener);
        // Create device picker adapter
        mPickerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, mPickerList);
        // Add listener for the buttons
        setButtonOnClickListener();
    }

    @Override
    protected void onPause() {
        mCurrentDevice = null;
        setPickerIconVisibility(false);
        mController.stop();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_install_client, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem flingButton = menu.findItem(R.id.menu_install);
        synchronized (mDeviceListAvailableLock) {
            if (mDeviceList.size() > 0) {
                if (mCurrentDevice != null) {
                    flingButton.setIcon(R.mipmap.ic_whisperplay_default_blue_light_24dp);
                } else {
                    flingButton.setIcon(R.mipmap.ic_whisperplay_default_light_24dp);
                }
                setPickerIconVisibility(true);
            } else {
                flingButton.setIcon(R.mipmap.ic_whisperplay_default_light_24dp);
                setPickerIconVisibility(false);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if (id == R.id.menu_install) {
            if (mCurrentDevice == null) {
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.menu_install))
                        .setAdapter(mPickerAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int index) {
                                mCurrentDevice = mDeviceList.get(index);
                                invalidateOptionsMenu();
                            }
                        })
                        .show();
                return true;
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(mCurrentDevice.getName())
                        .setNeutralButton(getString(R.string.btn_disconnect),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        mCurrentDevice = null;
                                        invalidateOptionsMenu();
                                        dialogInterface.dismiss();
                                    }
                                })
                        .show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setPickerIconVisibility(boolean enable) {
        Log.i(TAG, "setPickerIconVisibility: " + (enable ? "enable" : "disable"));
        MenuItem flingButton = mMenu.findItem(R.id.menu_install);
        flingButton.setVisible(enable);
    }

    private void setButtonOnClickListener() {
        mGetPackageVersionButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentDevice == null) {
                    new AlertDialog.Builder(InstallClientActivity.this)
                            .setTitle(getString(R.string.no_firetv_selected))
                            .setNeutralButton(getString(R.string.btn_close),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    })
                            .show();
                } else {
                    mCurrentDevice.getInstalledPackageVersion(mPacakgeNameEditText.getText().toString()).getAsync(
                            new RemoteInstallService.FutureListener<String>() {
                        @Override
                        public void futureIsNow(Future<String> future) {
                            try {
                                final String version = future.get();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AlertDialog.Builder(InstallClientActivity.this)
                                                .setTitle(getString(R.string.version) + version)
                                                .setNeutralButton(getString(R.string.btn_close),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.dismiss();
                                                            }
                                                        })
                                                .show();
                                    }
                                });
                            } catch (InterruptedException e) {
                                Log.e(TAG, "InterruptedException", e);
                            } catch (ExecutionException e) {
                                Log.e(TAG, "ExecutionException", e);
                            }
                        }
                    });
                }
            }
        });
        mInstallByASINButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentDevice == null) {
                    new AlertDialog.Builder(InstallClientActivity.this)
                            .setTitle(getString(R.string.no_firetv_selected))
                            .setNeutralButton(getString(R.string.btn_close),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    })
                            .show();
                } else {
                    mCurrentDevice.installByASIN(mInstallASINEditText.getText().toString().toUpperCase(Locale.ENGLISH))
                            .getAsync(new RemoteInstallService.FutureListener<Void>() {
                                @Override
                                public void futureIsNow(Future<Void> future) {
                                    try {
                                        future.get();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast toast = Toast.makeText(getApplicationContext(),
                                                        getString(R.string.remote_call_success), Toast.LENGTH_SHORT);
                                                toast.show();
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                        Log.e(TAG, "InterruptedException", e);
                                    } catch (ExecutionException e) {
                                        Log.e(TAG, "ExecutionException", e);
                                    }
                                }
                            });
                }
            }
        });
    }
    private static class RemoteInstallServiceComp implements Comparator<RemoteInstallService> {
        @Override
        public int compare(RemoteInstallService service1, RemoteInstallService service2) {
            return service1.getName().compareTo(service2.getName());
        }
    }
}
