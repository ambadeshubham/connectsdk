/**
 * FlingActivity.java
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms. 
 */

package com.amazon.whisperplay.example.flingrouteprovider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.media.MediaControlIntent;
import android.support.v7.media.MediaItemMetadata;
import android.support.v7.media.MediaItemStatus;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaSessionStatus;
import android.support.v7.media.RemotePlaybackClient;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazon.whisperplay.fling.provider.FireTVBuiltInReceiverMetadata;
import com.amazon.whisperplay.fling.provider.FlingMediaControlIntent;
import com.amazon.whisperplay.fling.provider.FlingMediaRouteProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FlingActivity extends Activity implements View.OnClickListener {

    // Debugging TAG
    private static final String TAG = FlingActivity.class.getName();

    // Lock object for mDeviceList synchronization
    private final Object mDeviceListAvailableLock = new Object();

    // List of device picker items.
    private List<String> mPickerList = new ArrayList<>();

    // Comparator to sort device list with alphabet device name order
    private RemoteMediaPlayerComp mComparator = new RemoteMediaPlayerComp();

    // Device picker adapter
    private ArrayAdapter<String> mPickerAdapter;

    // ListView for Media Source list
    private ListView mMediaListView;

    // TextView to show total and current duration as number
    private TextView mTotalDuration;
    private TextView mCurrentDuration;

    // Current Title and Media Status to show
    private TextView mCurrentStatusView;
    private TextView mMediaTitleView;
    private boolean mMediaTitleSet = false;

    // Playback buttons as ImageView
    private ImageView mBackwardButton;
    private ImageView mPlayButton;
    private ImageView mPauseButton;
    private ImageView mStopButton;
    private ImageView mForwardButton;

    // Progress(SeekBar) of media duration
    private SeekBar mSeekBar;
    private Long mMediaDuration = 0L;

    // MediaSource manager to load media information from external storage
    private MediaSourceManager mManager;

    // Application menu
    private Menu mMenu;

    // Error count for failing remote call
    private static final int MAX_ERRORS = 5;
    private int mErrorCount = 0;

    //status of the currently connected route
    private Status mStatus = new Status();
    private final Object mStatusLock = new Object();

    //currently connected route
    private MediaRouter.RouteInfo mCurrentRoute;

    //list of routes that can be connected to
    private List<android.support.v7.media.MediaRouter.RouteInfo> mDeviceList = new LinkedList<>();
    private List<android.support.v7.media.MediaRouter.RouteInfo> mPickerDeviceList = new LinkedList<>();

    //handler
    private final Handler mHandler = new Handler();

    //status listener of the currently playing media
    private Timer mStatusListener;

    //media router variables
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;

    //client for the current connection
    private RemotePlaybackClient mClient;

    //id for the currently playing media
    private String mPlayListItemID;

    //media info for the currently playing media
    private Bundle mediaInfo;

    private MediaRouter.Callback mMediaRouterCallback = new MediaRouter.Callback() {
        public void onRouteAdded(MediaRouter router, MediaRouter.RouteInfo route) {
            synchronized (mDeviceListAvailableLock) {
                if (mDeviceList.contains(route)) {
                    mDeviceList.remove(route);
                    Log.i(TAG, "Updating Device:" + route.getName());
                } else {
                    Log.i(TAG, "Adding Device:" + route.getName());
                }
                mDeviceList.add(route);

                triggerUpdate();
            }
        }

        public void onRouteRemoved(MediaRouter router, MediaRouter.RouteInfo route) {
            synchronized (mDeviceListAvailableLock) {
                if (mDeviceList.contains(route)) {
                    Log.i(TAG, "Removing Device:" + route.getName());

                    if (route.equals(mCurrentRoute)) {
                        if (mClient.hasSession()) {
                            stopStatusListenerTimer();
                            mClient.endSession(null, new RemotePlaybackClient.SessionActionCallback() {
                                @Override
                                public void onError(String error, int code, Bundle data) {
                                    handleFailure(new RuntimeException("Failed to end session"), error, true);
                                }
                            });
                        }
                        mCurrentRoute = null;
                    }
                    mDeviceList.remove(route);
                    triggerUpdate();
                }
            }
        }
    };

    void triggerUpdate() {
        // It should be run in main thread since it is updating Adapter.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPickerDeviceList = mDeviceList;
                Collections.sort(mPickerDeviceList, mComparator);
                mPickerList.clear();
                for (MediaRouter.RouteInfo device : mPickerDeviceList) {
                    mPickerList.add(device.getName());
                }
                mPickerAdapter.notifyDataSetChanged();
                // Calling onPrepareOptionsMenu() to update picker icon
                invalidateOptionsMenu();
            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hide Home icon
        getActionBar().setDisplayShowHomeEnabled(false);

        // Initialize UI resources
        mMediaTitleView = (TextView) findViewById(R.id.currentmediatitle);
        mCurrentStatusView = (TextView) findViewById(R.id.currentstatus);
        mCurrentDuration = (TextView) findViewById(R.id.currentDuration);
        mTotalDuration = (TextView) findViewById(R.id.totalDuration);
        mMediaListView = (ListView) findViewById(R.id.mediaList);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mBackwardButton = (ImageView) findViewById(R.id.backward);
        mPlayButton = (ImageView) findViewById(R.id.play);
        mPauseButton = (ImageView) findViewById(R.id.pause);
        mStopButton = (ImageView) findViewById(R.id.stop);
        mForwardButton = (ImageView) findViewById(R.id.forward);

        mMediaRouter = MediaRouter.getInstance(this);
        mMediaRouter.addProvider(new FlingMediaRouteProvider(this, "amzn.thin.pl"));

        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(MediaControlIntent.CATEGORY_REMOTE_PLAYBACK)
                .build();

        // Create MediaSourceManager
        mManager = new MediaSourceManager(this);
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onResume() {
        super.onResume();

        // Playback controller will be enabled when connectionUpdate succeed.
        setPlaybackControlWorking(false);

        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);

        // Set Adapter with media sources
        mMediaListView.setAdapter(new MediaListAdapter(this, mManager.getAllSources()));

        // Create device picker adapter
        mPickerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_activated_1, mPickerList);

        // When user selects media from listView, start fling directly.
        mMediaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                synchronized (mDeviceListAvailableLock) {
                    if (mCurrentRoute != null) {
                        Log.i(TAG, "setOnItemClickListener - Start fling.");
                        final ListView lv = (ListView) findViewById(R.id.mediaList);
                        MediaListAdapter ad = (MediaListAdapter)lv.getAdapter();
                        int selectedPosition = lv.getCheckedItemPosition();
                        if (selectedPosition >= 0) {
                            MediaSourceManager.MediaSource source =
                                    (MediaSourceManager.MediaSource)ad.getItem(selectedPosition);
                            Log.i(TAG, "setOnItemClickListener - Source =" + source);
                            Log.i(TAG, "setOnItemClickListener - Start fling:target:"
                                    + mCurrentRoute.toString());
                            JSONObject metadata = new JSONObject(source.metadata);
                            fling(source.url, metadata);
                        } else {
                            Log.i(TAG, "setOnItemClickListener - Select item first");
                        }
                    } else {
                        Log.i(TAG, "setOnItemClickListener - Target device is null");
                    }
                }
            }
        });

        // When user moves progress bar, seek absolute position from current player.
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mCurrentRoute != null) {
                    Log.i(TAG, "SeekBar(Absolute seek) - " + convertTime(seekBar.getProgress()));

                    mClient.seek(mPlayListItemID, seekBar.getProgress(), null, new RemotePlaybackClient.ItemActionCallback() {
                        @Override
                        public void onError(String error, int code, Bundle data) {
                            handleFailure(new RuntimeException("Error Seeking"), error, true);
                        }
                    });
                }
            }
        });

        // initialize error count
        mErrorCount = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    /**
     * @{inheritDoc
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem flingButton = menu.findItem(R.id.menu_fling);
        if (mPickerDeviceList.size() > 0) {
            if (mCurrentRoute != null) {
                flingButton.setIcon(R.drawable.ic_whisperplay_default_blue_light_24dp);
                setPlaybackControlWorking(true);
            } else {
                flingButton.setIcon(R.drawable.ic_whisperplay_default_light_24dp);
                setPlaybackControlWorking(false);
            }
            setPickerIconVisibility(true);
        } else {
            flingButton.setIcon(R.drawable.ic_whisperplay_default_light_24dp);
            setPickerIconVisibility(false);
            setPlaybackControlWorking(false);
        }
        return true;
    }

    /**
     * @{inheritDoc
     */
    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        if (mCurrentRoute != null) {
            if (mClient.hasSession()) {
                stopStatusListenerTimer();
                mClient.endSession(null, new RemotePlaybackClient.SessionActionCallback() {
                    @Override
                    public void onError(String error, int code, Bundle data) {
                        handleFailure(new RuntimeException("Failed to end session"), error, true);
                    }
                });
            }
            mMediaRouter.removeCallback(mMediaRouterCallback);
            clean();
        } else {
            clean();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    private void setStatusAndTitleVisibility(boolean enable) {
        Log.i(TAG, "setStatusAndTitleVisibility:" + (enable ? "enable" : "disable"));
        if (enable) {
            mCurrentStatusView.setVisibility(View.VISIBLE);
            mMediaTitleView.setVisibility(View.VISIBLE);
        } else {
            mCurrentStatusView.setVisibility(View.INVISIBLE);
            mMediaTitleView.setVisibility(View.INVISIBLE);
            mCurrentStatusView.setText(getString(R.string.empty_text));
            mMediaTitleView.setText(getString(R.string.empty_text));
        }
    }

    private void resetMediaTitle() {
        Log.i(TAG, "Resetting Media Title");
        mMediaTitleSet = false;
        mMediaTitleView.setText(getString(R.string.empty_text));
    }

    private void resetDuration() {
        Log.i(TAG, "resetDuration");
        mMediaDuration = 0L;
        mSeekBar.setProgress(0);
        mSeekBar.setMax(0);
        mCurrentDuration.setText(convertTime(0));
        mTotalDuration.setText(convertTime(0));
    }

    private void setPlaybackControlWorking(boolean enable) {
        Log.i(TAG, "setPlaybackControlWorking:" + (enable ? "enable" : "disable"));
        mPlayButton.setEnabled(enable);
        mPauseButton.setEnabled(enable);
        mStopButton.setEnabled(enable);
        mForwardButton.setEnabled(enable);
        mBackwardButton.setEnabled(enable);
    }

    private void setProgressVisibility(boolean enable) {
        Log.i(TAG, "setProgressVisibility:" + (enable ? "enable" : "disable"));
        if (enable) {
            mSeekBar.setVisibility(View.VISIBLE);
            mCurrentDuration.setVisibility(View.VISIBLE);
            mTotalDuration.setVisibility(View.VISIBLE);

        } else {
            mCurrentDuration.setText(convertTime(0));
            mTotalDuration.setText(convertTime(0));
            mSeekBar.setMax(0);
            mSeekBar.setProgress(0);
            mSeekBar.setVisibility(View.INVISIBLE);
            mCurrentDuration.setVisibility(View.INVISIBLE);
            mTotalDuration.setVisibility(View.INVISIBLE);
        }
    }

    private void setPickerIconVisibility(boolean enable) {
        Log.i(TAG, "setPickerIconVisibility: " + (enable ? "enable" : "disable"));
        MenuItem flingButton = mMenu.findItem(R.id.menu_fling);
        flingButton.setVisible(enable);
    }

    @Override
    public void onClick(View view) {
        int state;
        synchronized (mStatusLock) {
            state = mStatus.mState;
        }
        switch (view.getId()) {

            case R.id.play:
                Log.i(TAG, "onClick - PlayButton");
                if (state == MediaItemStatus.PLAYBACK_STATE_PAUSED || state == MediaItemStatus.PLAYBACK_STATE_PENDING) {
                    Log.i(TAG, "onClick - Start doPlay");
                    doResume();
                } else {
                    synchronized (mDeviceListAvailableLock) {
                        if (mCurrentRoute != null) {
                            Log.i(TAG, "onClick - Enter");
                            final ListView lv = (ListView) findViewById(R.id.mediaList);
                            MediaListAdapter ad = (MediaListAdapter)lv.getAdapter();
                            int position = lv.getCheckedItemPosition();
                            if (position >= 0) {
                                MediaSourceManager.MediaSource source =
                                        (MediaSourceManager.MediaSource) ad.getItem(position);
                                Log.i(TAG, "onClick - Source =" + source);
                                JSONObject metadata = new JSONObject(source.metadata);
                                Log.i(TAG, "onClick - fling");
                                fling(source.url, metadata);
                            } else {
                                Log.i(TAG, "onClick - Media must be selected first.");
                            }
                            Log.i(TAG, "onClick - Exit");
                        } else {
                            Log.i(TAG, "onClick - Target device is null");
                        }
                    }
                }
                break;
            case R.id.pause:
                Log.i(TAG, "onClick - PauseButton");
                doPause();
                break;
            case R.id.stop:
                Log.i(TAG, "onClick - StopButton");
                doStop();
                break;
            case R.id.forward:
                Log.i(TAG, "onClick - ForwardButton");
                doFore();
                break;
            case R.id.backward:
                Log.i(TAG, "onClick - BackwardButton");
                doBack();
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if (id == R.id.menu_fling) {
            if (mCurrentRoute == null) {
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.menu_fling))
                        .setAdapter(mPickerAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int index) {
                                connectionUpdate(mPickerDeviceList.get(index));
                            }
                        })
                        .show();
                return true;
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(mCurrentRoute.getName())
                        .setNeutralButton(getString(R.string.btn_disconnect),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        connectionUpdate(null);
                                        dialogInterface.dismiss();
                                    }
                                })
                        .show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void connectionUpdate(final MediaRouter.RouteInfo target) {
        new ConnectionUpdateTask().execute(target);
    }

    private void setStatusText() {
        // This method deals with UI on main thread.
        runOnUiThread(new Runnable() {
            public void run() {
                synchronized (mStatusLock) {
                    switch (mStatus.mState) {
                        case MediaItemStatus.PLAYBACK_STATE_BUFFERING:
                            Log.i(TAG, "setStatusText - Buffering");
                            mCurrentStatusView.setText(getString(R.string.media_preping));
                            break;
                        case MediaItemStatus.PLAYBACK_STATE_PENDING:
                            Log.i(TAG, "setStatusText - Pending");
                            mCurrentStatusView.setText(getString(R.string.media_readytoplay));
                            break;
                        case MediaItemStatus.PLAYBACK_STATE_PLAYING:
                            Log.i(TAG, "setStatusText - Playing");
                            if (!mMediaTitleSet) {
                                UpdateMediaTitle();
                            }
                            mCurrentStatusView.setText(getString(R.string.media_playing));
                            setProgressVisibility(true);
                            setStatusAndTitleVisibility(true);
                            break;
                        case MediaItemStatus.PLAYBACK_STATE_PAUSED:
                            Log.i(TAG, "setStatusText - Paused");
                            if (!mMediaTitleSet) {
                                UpdateMediaTitle();
                            }
                            mCurrentStatusView.setText(getString(R.string.media_paused));
                            setProgressVisibility(true);
                            setStatusAndTitleVisibility(true);
                            break;
                        case MediaItemStatus.PLAYBACK_STATE_FINISHED:
                            Log.i(TAG, "setStatusText - Finished");
                            mCurrentStatusView.setText(getString(R.string.media_done));
                            resetDuration();
                            break;
                        case MediaItemStatus.PLAYBACK_STATE_ERROR:
                            Log.i(TAG, "setStatusText - Error");
                            mCurrentStatusView.setText(getString(R.string.media_error));
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    private void handleFailure( Throwable throwable, final String msg, final boolean extend ) {
        Log.e(TAG, msg, throwable);
        final String exceptionMessage = throwable.getMessage();

        mErrorCount = mErrorCount+1;
        if (mErrorCount > MAX_ERRORS) {
            errorMessagePopup(msg + (extend ? exceptionMessage : ""));
        }
    }

    private void errorMessagePopup(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "errorMessagePopup: Showing the error message.");
                new AlertDialog.Builder(FlingActivity.this)
                        .setTitle(getString(R.string.communication_error))
                        .setMessage(message)
                        .setNeutralButton(getString(R.string.btn_close),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                        .show();
                if (mCurrentRoute != null) {
                    if (mClient.hasSession()) {
                        stopStatusListenerTimer();
                        mClient.endSession(null, new RemotePlaybackClient.SessionActionCallback() {
                            @Override
                            public void onError(String error, int code, Bundle data) {
                                handleFailure(new RuntimeException("Failed to end session"), error, true);
                            }
                        });
                    }
                    mCurrentRoute = null;
                }
                resetDuration();
                setStatusAndTitleVisibility(false);
                setPlaybackControlWorking(false);
                setProgressVisibility(false);
                invalidateOptionsMenu();
                resetMediaTitle();
            }
        });
    }

    private void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void fling(final String name, final JSONObject metadata) {
        mStatus.clear();
        resetDuration();
        resetMediaTitle();

        String mimeType = "";
        Bundle metadataBundle = new Bundle();

        try {
            Iterator<?> keys = metadata.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (key.equalsIgnoreCase("title")) {
                    metadataBundle.putString(MediaItemMetadata.KEY_TITLE, metadata.getString(key));
                } else if (key.equalsIgnoreCase("poster")) {
                    metadataBundle.putString(MediaItemMetadata.KEY_ARTWORK_URI, metadata.getString(key));
                } else if (key.equalsIgnoreCase("type")) {
                    mimeType = metadata.getString(key);
                    metadataBundle.putString(FireTVBuiltInReceiverMetadata.KEY_TYPE, metadata.getString(key));
                } else if (key.equalsIgnoreCase("description")) {
                    metadataBundle.putString(FireTVBuiltInReceiverMetadata.KEY_DESCRIPTION, metadata.getString(key));
                } else if (key.equalsIgnoreCase("tracks") && metadata.get(key) instanceof JSONArray) {
                    ArrayList<Bundle> tracks = new ArrayList<>();
                    JSONArray trackArray = (JSONArray) metadata.get(key);
                    for (int i = 0; i < trackArray.length(); i++) {
                        Bundle trackBundle = new Bundle();
                        JSONObject trackJSON = trackArray.getJSONObject(i);
                        Iterator<?> tracksKeys = trackJSON.keys();
                        while (tracksKeys.hasNext()) {
                            String trackKey = (String) tracksKeys.next();
                            if (trackKey.equalsIgnoreCase("src")) {
                                trackBundle.putString(FireTVBuiltInReceiverMetadata.KEY_TRACK_SOURCE, trackJSON.getString(trackKey));
                            } else if (trackKey.equalsIgnoreCase("kind")) {
                                trackBundle.putString(FireTVBuiltInReceiverMetadata.KEY_TRACK_KIND, trackJSON.getString(trackKey));
                            } else if (trackKey.equalsIgnoreCase("srclang")) {
                                trackBundle.putString(FireTVBuiltInReceiverMetadata.KEY_TRACK_SOURCE_LANGUAGE, trackJSON.getString(trackKey));
                            } else if (trackKey.equalsIgnoreCase("label")) {
                                trackBundle.putString(FireTVBuiltInReceiverMetadata.KEY_TRACK_LABEL, trackJSON.getString(trackKey));
                            }
                        }
                        tracks.add(trackBundle);
                    }

                    metadataBundle.putParcelableArrayList(FireTVBuiltInReceiverMetadata.KEY_TRACKS, tracks);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException reading metadata" + e);
        }

        mClient.play(Uri.parse(name), mimeType, metadataBundle, 0, null, new RemotePlaybackClient.ItemActionCallback() {
            @Override
            public void onResult(Bundle data, String sessionId, MediaSessionStatus sessionStatus,
                                 String itemId, MediaItemStatus itemStatus) {
                mPlayListItemID = itemId;
                startStatusListenerTimer();
            }

            @Override
            public void onError(String error, int code, Bundle data) {
                handleFailure(new RuntimeException("Failed to fling media"), error, true);
            }
        });

        showToast("try Flinging...");
    }

    private void doResume() {
        if (mClient != null) {
            mClient.resume(null, new RemotePlaybackClient.SessionActionCallback() {
                @Override
                public void onError(String error, int code, Bundle data) {
                    handleFailure(new RuntimeException("Failed to resume media"), error, true);
                }
            });
        }
    }

    private void doPause() {
        if (mClient != null) {
            mClient.pause(null, new RemotePlaybackClient.SessionActionCallback() {
                @Override
                public void onError(String error, int code, Bundle data) {
                    handleFailure(new RuntimeException("Failed to pause media"), error, true);
                }
            });
        }
    }

    private void doStop() {
        if (mClient != null) {
            mClient.stop(null, new RemotePlaybackClient.SessionActionCallback() {
                @Override
                public void onResult(android.os.Bundle data, java.lang.String sessionId, android.support.v7.media.MediaSessionStatus sessionStatus) {
                    mStatus.clear();
                    resetDuration();
                }

                @Override
                public void onError(String error, int code, Bundle data) {
                    handleFailure(new RuntimeException("Failed to stop media"), error, true);
                }
            });
        }
    }

    private void doFore() {
        if (mClient != null) {
            mClient.seek(mPlayListItemID, mStatus.mPosition + 10000, null, new RemotePlaybackClient.ItemActionCallback() {
                @Override
                public void onError(String error, int code, Bundle data) {
                    handleFailure(new RuntimeException("Failed to seek forward media"), error, true);
                }
            });
        }
    }

    private void doBack() {
        if (mClient != null) {
            mClient.seek(mPlayListItemID, mStatus.mPosition - 10000, null, new RemotePlaybackClient.ItemActionCallback() {
                @Override
                public void onError(String error, int code, Bundle data) {
                    handleFailure(new RuntimeException("Failed to seek backward media"), error, true);
                }
            });
        }
    }

    private static String convertTime(long time) {
        long totalSecs = time / 1000;
        long hours = totalSecs / 3600;
        long minutes = (totalSecs / 60) % 60;
        long seconds = totalSecs % 60;
        String hourString = (hours == 0) ? "00" : ((hours < 10) ? "0" + hours : "" + hours);
        String minString = (minutes == 0) ? "00" : ((minutes < 10) ? "0" + minutes : "" + minutes);
        String secString = (seconds == 0) ? "00" : ((seconds < 10) ? "0" + seconds : "" + seconds);

        return hourString + ":" + minString + ":" + secString;
    }

    private void clean() {
        mCurrentRoute = null;
        mDeviceList.clear();
        mPickerDeviceList.clear();
        resetDuration();
        resetMediaTitle();
        setStatusAndTitleVisibility(false);
        setProgressVisibility(false);
        setPickerIconVisibility(false);
        setPlaybackControlWorking(false);
    }

    private static class Status {
        public long mPosition;
        public int mState;

        public synchronized void clear() {
            mPosition = -1L;
            mState = MediaItemStatus.PLAYBACK_STATE_PENDING;
        }
    }

    private class ConnectionUpdateTask extends AsyncTask<MediaRouter.RouteInfo, Void, Integer> {
        @Override
        protected Integer doInBackground(MediaRouter.RouteInfo... remoteMediaPlayers) {
            int threadId = android.os.Process.myTid();
            if (remoteMediaPlayers[0] != null) { // Connect
                MediaRouter.RouteInfo target = remoteMediaPlayers[0];
                if (mClient != null && mClient.hasSession()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopStatusListenerTimer();
                            mClient.endSession(null, new RemotePlaybackClient.SessionActionCallback() {
                                @Override
                                public void onError(String error, int code, Bundle data) {
                                    handleFailure(new RuntimeException("Failed to end session"), error, true);
                                }
                            });
                        }
                    });
                }

                //start session with the new route
                mCurrentRoute = target;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCurrentRoute.select();
                        mClient = new RemotePlaybackClient(getApplicationContext(), mCurrentRoute);
                        mClient.startSession(null, new RemotePlaybackClient.SessionActionCallback() {
                            @Override
                            public void onError(String error, int code, Bundle data) {
                                handleFailure(new RuntimeException("Failed to start session"), error, true);
                            }
                        });
                    }
                });
                Log.i(TAG, "[" + threadId + "]" + "ConnectionUpdateTask:set current device"
                        + ":currentDevice=" + target);
                return 0;
            } else { // Disconnect
                if (mClient.hasSession()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopStatusListenerTimer();
                            mClient.endSession(null, new RemotePlaybackClient.SessionActionCallback() {
                                @Override
                                public void onError(String error, int code, Bundle data) {
                                    handleFailure(new RuntimeException("Failed to end session"), error, true);
                                }
                            });
                        }
                    });
                }
                return 1;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            MenuItem item = mMenu.findItem(R.id.menu_fling);
            switch (result) {
                case 0:
                    // after connection
                    mErrorCount = 0;
                    Log.i(TAG, "[main]" + "ConnectionUpdateTask:onPostExecute: connection");
                    item.setIcon(R.drawable.ic_whisperplay_default_blue_light_24dp);
                    invalidateOptionsMenu();
                    break;
                case 1:
                    // after disconnection
                    Log.i(TAG, "[main]" + "ConnectionUpdateTask:onPostExecute: disconnection");
                    item.setIcon(R.drawable.ic_whisperplay_default_light_24dp);
                    mCurrentRoute = null;
                    invalidateOptionsMenu();
                    setProgressVisibility(false);
                    setStatusAndTitleVisibility(false);
                    resetDuration();
                    resetMediaTitle();
                    break;
            }
        }
    }

    private void UpdateMediaTitle() {
        int threadId = android.os.Process.myTid();
        if (mCurrentRoute != null) {
            Log.i(TAG, "[" + threadId + "]" + "UpdateMediaTitle:getMediaInfo");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.addCategory(MediaControlIntent.CATEGORY_REMOTE_PLAYBACK);
                    intent.setAction(FlingMediaControlIntent.ACTION_GET_MEDIA_INFO);
                    mCurrentRoute.sendControlRequest(intent, new MediaRouter.ControlRequestCallback() {
                        @Override
                        public void onResult(Bundle data) {
                            mediaInfo = data.getBundle(MediaControlIntent.EXTRA_ITEM_METADATA);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mMediaTitleView.setText(mediaInfo.getString(MediaItemMetadata.KEY_TITLE));
                                    setStatusAndTitleVisibility(true);
                                    mMediaTitleSet = true;
                                }
                            });
                        }
                    });
                }
            });
        }
    }

    private static class RemoteMediaPlayerComp implements Comparator<MediaRouter.RouteInfo> {
        @Override
        public int compare(MediaRouter.RouteInfo player1, MediaRouter.RouteInfo player2) {
            return player1.getName().compareTo(player2.getName());
        }
    }

    private void stopStatusListenerTimer() {
        Log.d(TAG, "Stopped status listener timer task");
        if (mStatusListener != null) {
            mStatusListener.cancel();
        }
    }

    private void startStatusListenerTimer() {
        stopStatusListenerTimer();
        mStatusListener = new Timer();
        mStatusListener.scheduleAtFixedRate(new StatusListenerTask(), 100, 1000);
        Log.d(TAG, "Started status listener timer task");
    }

    private class StatusListenerTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
               @Override
                public void run() {
                   if (mClient != null && mClient.hasSession()) {
                       mClient.getStatus(mPlayListItemID, null, new RemotePlaybackClient.ItemActionCallback() {

                           @Override
                           public void onResult(final Bundle data, String sessionId, MediaSessionStatus sessionStatus, String itemId, MediaItemStatus itemStatus) {
                               synchronized (mStatusLock) {
                                   mStatus.mState = MediaItemStatus.fromBundle(data.getBundle(MediaControlIntent.EXTRA_ITEM_STATUS)).getPlaybackState();
                                   mStatus.mPosition = MediaItemStatus.fromBundle(data.getBundle(MediaControlIntent.EXTRA_ITEM_STATUS)).getContentPosition();
                                   mMediaDuration = MediaItemStatus.fromBundle(data.getBundle(MediaControlIntent.EXTRA_ITEM_STATUS)).getContentDuration();
                               }
                               if (mMediaDuration > 0) {
                                   runOnUiThread(new Runnable() {
                                       @Override
                                       public void run() {
                                           mTotalDuration.setText(String.valueOf(convertTime(mMediaDuration)));
                                           mSeekBar.setMax(mMediaDuration.intValue());
                                           mCurrentDuration.setText(String.valueOf(
                                                   convertTime(mStatus.mPosition)));
                                           mSeekBar.setProgress((int) mStatus.mPosition);
                                       }
                                   });
                               }
                               setStatusText();
                           }

                           @Override
                           public void onError(String error, int code, Bundle data) {
                               handleFailure(new RuntimeException("Failed to get status"), error, true);
                           }

                       });
                   }
               }
            });
        }
    }
}