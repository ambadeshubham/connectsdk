/**
 * RootViewController.m
 *
 * Copyright (c) 2015-2017 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import "RootViewController.h"

#define AMAZON_FLING_PLAYER_ASIN @"B075FKD4LC"
#define AMAZON_FLING_PLAYER_SID  @"amzn.fling.sample.customplayer"

@interface FireTV : NSObject

@property id<RemoteInstallService> installService;
@property id<RemoteMediaPlayer> player;

@end

@implementation FireTV

@end

@interface RootViewController ()

@property DiscoveryController *controller;
@property InstallDiscoveryController *installController;
@property NSMutableDictionary *deviceMap;
@property NSArray *presentedDevices;

@end

@implementation RootViewController

#pragma mark - View Controller LifeCycle

// -------------------------------------------------------------------------------
//	viewDidLoad
// -------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(suspend)
                                                 name: @"willResignActive"
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(resume)
                                                 name: @"willEnterForegound"
                                               object: nil];

    self.deviceMap = [NSMutableDictionary dictionary];
    self.controller = [[DiscoveryController alloc] init];
    self.installController = [[InstallDiscoveryController alloc] init];
    self.playerController.player = nil;
    self.picker.image = nil;
    [self.controller searchPlayerWithId:AMAZON_FLING_PLAYER_SID andListener:self];
    [self.installController searchInstallServiceWithListener:self];
}

// -------------------------------------------------------------------------------
//	viewWillLayoutSubviews
// -------------------------------------------------------------------------------
- (void)viewWillLayoutSubviews {
    [self setSubviewFrames:(self.playerController.player != nil)];
    [self.view layoutSubviews];
}

// -------------------------------------------------------------------------------
//	didReceiveMemoryWarning
// -------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UI code

// -------------------------------------------------------------------------------
//	updateUI:playerControlDisplayed
// -------------------------------------------------------------------------------
- (void)updateUI:(BOOL) playerControlDisplayed {
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI depending on the state (player selected/not selected)
        [UIView transitionWithView:self.playerController.view
                          duration:0.4
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:NULL
                        completion:NULL];
        if (playerControlDisplayed) {
            self.playerController.view.hidden = NO;
            self.picker.image = [[UIImage imageNamed:@"PickerConnected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        } else {
            self.playerController.view.hidden = YES;
            self.picker.image = [[UIImage imageNamed:@"Picker"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [self.mediaList.tableView deselectRowAtIndexPath:[self.mediaList.tableView indexPathForSelectedRow] animated:NO];
        }
        
        [self setSubviewFrames:playerControlDisplayed];
        [self.mediaList viewDidLoad];
    });
}

// -------------------------------------------------------------------------------
//	setSubviewFrames:playerControlDisplayed
// -------------------------------------------------------------------------------
- (void)setSubviewFrames:(BOOL) playerControlDisplayed {
    CGRect mediaListFrame = self.mediaList.view.frame;
    CGRect playerFrame = self.playerController.view.frame;
    CGFloat barHeight = self.navigationController.navigationBar.frame.origin.y +
    self.navigationController.navigationBar.frame.size.height;
    
    mediaListFrame.origin.y = barHeight;
    mediaListFrame.size.width = self.view.frame.size.width;
    
    playerFrame.origin.y = self.view.frame.size.height - playerFrame.size.height;
    playerFrame.size.width = self.view.frame.size.width;
    playerFrame.size.height = 158; // FIXME - to a more generic value
    
    if (playerControlDisplayed) {
        mediaListFrame.size.height = self.view.frame.size.height - barHeight - playerFrame.size.height;
    } else {
        mediaListFrame.size.height = self.view.frame.size.height - barHeight;
    }
    
    self.mediaList.view.frame = mediaListFrame;
    self.playerController.view.frame = playerFrame;
}

// -------------------------------------------------------------------------------
//	showNetworkIndicator:visible
// -------------------------------------------------------------------------------
- (void)showNetworkIndicator : (BOOL) visible {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = visible;
    });
}

#pragma mark - Fling LifeCycle

// -------------------------------------------------------------------------------
//	resume
// -------------------------------------------------------------------------------
- (void)resume {
    NSLog(@"$$resume");
    [self.controller resume];
    [self.installController resume];
}

// -------------------------------------------------------------------------------
//	suspend
// -------------------------------------------------------------------------------
- (void)suspend {
    NSLog(@"$$suspend");
    dispatch_async(dispatch_get_main_queue(), ^{
        self.picker.image = nil;
    });
    if (self.playerController.player != nil) {
        NSLog(@"$$suspend - Ensure removing listener before close");
        [self.playerController.player removeStatusListener:_playerController];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.deviceMap removeAllObjects];
    [self.controller close];
    [self.installController close];
}

#pragma mark - Session management

// -------------------------------------------------------------------------------
//	storeLastPlayer
// -------------------------------------------------------------------------------
- (void)storeLastPlayer {
    if (self.playerController.player != nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[self.playerController.player uniqueIdentifier] forKey:@"lastPlayerId"];
    }
}

// -------------------------------------------------------------------------------
//	forgetLastPlayer
// -------------------------------------------------------------------------------
- (void)forgetLastPlayer {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastPlayerId"];
}

// -------------------------------------------------------------------------------
//	rejoinLastPlayer:device
// -------------------------------------------------------------------------------
- (void)rejoinLastPlayer: (id<RemoteMediaPlayer>)device {
    NSString* lastPlayerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastPlayerId"];
    NSLog(@"Trying to rejoin %@", lastPlayerId);
    if ([[device uniqueIdentifier] isEqualToString:lastPlayerId]) {
        NSLog(@"$$rejoinLastPlayer:%@", device);
        // Connect to the device as it is available
        self.playerController.player = device;
    }
}

#pragma mark - Discovery

// -------------------------------------------------------------------------------
//	deviceDiscovered:device
// -------------------------------------------------------------------------------
- (void)deviceDiscovered:(id<RemoteMediaPlayer>)device {
    NSLog(@"Adding Device: %@", [device name]);
    FireTV* tv = [self.deviceMap objectForKey:[device uniqueIdentifier]];
    if (tv == nil) {
        tv = [[FireTV alloc] init];
        tv.player = device;
        tv.installService = nil;
        [self.deviceMap setObject:tv forKey:[device uniqueIdentifier]];
    } else {
        if (tv.player != nil) {
            // Handle possible duplicate
        } else {
            tv.player = device;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.picker.image = self.playerController.player != nil ?
            [[UIImage imageNamed:@"PickerConnected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] :
            [[UIImage imageNamed:@"Picker"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    });
    [self rejoinLastPlayer:device];
}

// -------------------------------------------------------------------------------
//	deviceLost:device
// -------------------------------------------------------------------------------
- (void)deviceLost:(id<RemoteMediaPlayer>)device {
    NSLog(@"deviceLost=%@", [device name]);
    FireTV* tv = [self.deviceMap objectForKey:[device uniqueIdentifier]];
    if (tv != nil) {
        if (self.playerController.player == device) {
            // Disconnect
            self.playerController.player = nil;
            [self updateUI:NO];
            [self showNetworkIndicator:NO];
        }
        tv.player = nil;
    }
    if (tv.player == nil && tv.installService == nil) {
        [self.deviceMap removeObjectForKey:[device uniqueIdentifier]];
    }
}

// -------------------------------------------------------------------------------
//	installServiceDiscovered:device
// -------------------------------------------------------------------------------
- (void)installServiceDiscovered:(id<RemoteInstallService>)device {
    NSLog(@"Adding Device: %@", [device name]);
    FireTV* tv = [self.deviceMap objectForKey:[device uniqueIdentifier]];
    if (tv == nil) {
        tv = [[FireTV alloc] init];
        tv.installService = device;
        tv.player = nil;
        [self.deviceMap setObject:tv forKey:[device uniqueIdentifier]];
    } else {
        if (tv.installService != nil) {
            // Handle possible duplicate
        } else {
            tv.installService = device;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        self.picker.image = self.playerController.player != nil ?
        [[UIImage imageNamed:@"PickerConnected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] :
        [[UIImage imageNamed:@"Picker"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    });
}

// -------------------------------------------------------------------------------
//	installServiceLost:device
// -------------------------------------------------------------------------------
- (void)installServiceLost:(id<RemoteInstallService>)device {
    NSLog(@"installServiceLost=%@", [device name]);
    FireTV* tv = [self.deviceMap objectForKey:[device uniqueIdentifier]];
    if (tv != nil) {
        tv.installService = nil;
    }
    if (tv.player == nil && tv.installService == nil) {
        [self.deviceMap removeObjectForKey:[device uniqueIdentifier]];
    }
}

// -------------------------------------------------------------------------------
//	discoveryFailure
// -------------------------------------------------------------------------------
- (void)discoveryFailure {
}

#pragma mark - Protocol Method Implementation

// -------------------------------------------------------------------------------
//	mediaSourceSelected:
// -------------------------------------------------------------------------------
- (void)mediaSourceSelected:(MediaSource*) source {
    NSLog(@"User selected %@", source.title);
    if (self.playerController.player == nil) {
        [self.mediaList.tableView deselectRowAtIndexPath:[self.mediaList.tableView indexPathForSelectedRow] animated:NO];
    } else {
        // Fling source.url, source.metadata
        self.playerController.statusLabel.text = @"Flinging...";
        NSError* error;
        NSData* data = [NSJSONSerialization dataWithJSONObject:source.metadata options:NSJSONWritingPrettyPrinted error:&error];
        NSString* metadataStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [self.playerController setMediaSource:source.url :metadataStr];
    }
}

// -------------------------------------------------------------------------------
//	connected:
// -------------------------------------------------------------------------------
- (void)connected {
    [self storeLastPlayer];
    [self updateUI:YES];
    [self showNetworkIndicator:NO];
}

// -------------------------------------------------------------------------------
//	disconnected:
// -------------------------------------------------------------------------------
- (void)disconnected {
    [self forgetLastPlayer];
    [self updateUI:NO];
    [self showNetworkIndicator:NO];
}

// -------------------------------------------------------------------------------
//	connectionFailure:
// -------------------------------------------------------------------------------
- (void)connectionFailure {
    // Clear the player control UI
    self.playerController.player = nil;
    [self updateUI:NO];
}

#pragma mark - UI helpers

// -------------------------------------------------------------------------------
//	Helper methods to show the error dialog
// -------------------------------------------------------------------------------
- (void)showErrorDialog {
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not Available", nil)
                               message:NSLocalizedString(@"The device is currently unavailable. Please try again.", nil)
                              delegate:nil
                     cancelButtonTitle:NSLocalizedString(@"OK", nil)
                     otherButtonTitles:nil];
    [alert show];
}

// -------------------------------------------------------------------------------
//	Helper method to show installation help dialog
// -------------------------------------------------------------------------------
- (void)showInstallingDialog {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Fling Install Sample", nil)
                                   message:NSLocalizedString(@"The app installation is launched on this Fire TV. Please use your Fire TV Remote to complete the installation on the Fire TV.", nil)
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(@"OK", nil)
                         otherButtonTitles:nil];
        [alert show];
    });
}

#pragma mark - Event Handlers

// -------------------------------------------------------------------------------
//	actionSheet:actionSheet buttonIndex
// -------------------------------------------------------------------------------
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [actionSheet cancelButtonIndex]) {
        // Clear the presented devices array if the user cancels
        if (self.presentedDevices != nil) {
            self.presentedDevices = nil;
        }
        return;
    }
    if (self.playerController.player == nil) {
        // Synchronizing current list of devices and the presented devices.
        // Make sure the selected device is still available
        if (self.presentedDevices != nil) {
            // Check if the selected item is still available
            if (buttonIndex < (NSInteger)[self.presentedDevices count]) {
                FireTV* selectedDevice = [self.presentedDevices objectAtIndex:buttonIndex];
                if (selectedDevice.player != nil) {
                    // Ensure that the user did not uninstall the application on the Fire TV
                    if ([self.deviceMap objectForKey:[selectedDevice.player uniqueIdentifier]] != nil) {
                        self.playerController.player = selectedDevice.player;
                        [self showNetworkIndicator:YES];
                    } else {
                        // Show error message
                        [self showErrorDialog];
                    }
                } else if (selectedDevice.installService != nil) {
                    if ([self.deviceMap objectForKey:[selectedDevice.installService uniqueIdentifier]] != nil) {
                        [[selectedDevice.installService installByASIN:AMAZON_FLING_PLAYER_ASIN] continueWithBlock:^id(BFTask *task) {
                            // Show alert that the installation or launch has started depending on the installed flag
                            if (task.error == nil) {
                                // Show 'installing' message
                                [self showInstallingDialog];
                            }
                            return nil;
                        }];
                    } else {
                        // Show error message
                        [self showErrorDialog];
                    }
                }
            }
            self.presentedDevices = nil;
        }
    } else {
        if (buttonIndex == 0) { // Disconnect
            NSLog(@"Disconnecting device:%@", [self.playerController.player name]);
            self.playerController.player = nil;
            [self showNetworkIndicator:YES];
        }
    }
    
    if (self.playerController.player == nil) {
        [self.mediaList.tableView deselectRowAtIndexPath:[self.mediaList.tableView indexPathForSelectedRow] animated:NO];
    }
}

// -------------------------------------------------------------------------------
//	onPickerClicked:
// -------------------------------------------------------------------------------
- (IBAction)onPickerClicked:(id)sender {
    // Show appropriate UI depending on state
    if (self.playerController.player == nil) {
        // Copy current list of devices into presented devices array. These two arrays
        // can go out of sync while the action sheet is displayed. The synchronization
        // is handled in UIActionSheetDelegate::clickedButtonAtIndex
        if (self.presentedDevices != nil) {
            self.presentedDevices = nil;
        }
        self.presentedDevices = [self.deviceMap allValues];
        UIActionSheet *actionSheet =
        [[UIActionSheet alloc] initWithTitle:@"Connect to Device"
                                    delegate:self
                           cancelButtonTitle:nil
                      destructiveButtonTitle:nil
                           otherButtonTitles:nil];
        
        // Present all, indicate those with the Fling Sample Player
        for (FireTV* tv in self.presentedDevices) {
            NSString* name = tv.installService ? [tv.installService name] : [tv.player name];
            NSString* title = name.length > 20 ?
                [[name substringToIndex:20] stringByAppendingString:@"..."] :
                [name stringByPaddingToLength:23 withString:@" " startingAtIndex:0];
            NSString* pickerRow = [NSString stringWithFormat:@"%@   %@", title, tv.player != nil ? @"✅" : @"⬇"];
            [actionSheet addButtonWithTitle:pickerRow];
        }
        
        [actionSheet addButtonWithTitle:@"Cancel"];
        actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
        [actionSheet showInView:self.view];
    } else {
        UIActionSheet *actionsheet = [[UIActionSheet alloc] init];
        actionsheet.title = [NSString stringWithFormat:@"Connected: %@",[self.playerController.player name]];
        actionsheet.delegate = self;
        [actionsheet addButtonWithTitle:@"Disconnect"];
        [actionsheet addButtonWithTitle:@"Cancel"];
        actionsheet.destructiveButtonIndex = 0;
        actionsheet.cancelButtonIndex = 1;
        [actionsheet showInView:self.view];
    }
}

@end
