/**
 * PlayerViewController.m
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import "PlayerViewController.h"

// Subclassed UIButton to show highlighted color when the user taps it
@interface HighlightedUIButton : UIButton

- (void) setHighlighted:(BOOL)highlighted;

@end

@implementation HighlightedUIButton

// -------------------------------------------------------------------------------
// setHighlighted
// Overridden to change the higlight color to tint color
// -------------------------------------------------------------------------------
- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = [self tintColor];
    }
    else {
        self.backgroundColor = self.superview.backgroundColor;
    }
}

@end

#define MAX_ERRORS 5

// View controller for the player view
@interface PlayerViewController ()

// the queue to run our Remote Media Player status updates
@property (nonatomic, strong) dispatch_queue_t statusUpdateQueue;

@property long errorCount;

@property long long durationOfClip;

@end

@implementation PlayerViewController

#pragma mark - View Controller Lifecycle

// -------------------------------------------------------------------------------
// viewDidLoad
// -------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Create queue
    self.statusUpdateQueue = dispatch_queue_create("playerViewController.status.updates", NULL);
}

// -------------------------------------------------------------------------------
// didReceiveMemoryWarning
// -------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setting Player

@synthesize player = _player;

// -------------------------------------------------------------------------------
// connectToPlayer
// -------------------------------------------------------------------------------
- (void)connectToPlayer {
    NSLog(@"Connecting to player:%@", [self.player name]);
    [[self.player addStatusListener:self] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Failed to add status listener.  Error:%@", task.error);
            [self handleError:task.error];
        } else {
            if (self.observer != nil) {
                [self.observer connected];
            }
            // Enable the UI
            [self synchronizeUI];
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// disconnectFromPlayer
// -------------------------------------------------------------------------------
- (void)disconnectFromPlayer:(void (^)(void))callbackBlock {
    NSLog(@"Disconnecting from player:%@", [self.player name]);
    [[self.player removeStatusListener:self] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Failed to remove status listener.  Error:%@", task.error);
        }
        // Ignore any error
        // Set player to nil
        _player = nil;
        if (self.observer != nil) {
            [self.observer disconnected];
        }
        // Call the callback once disconnected
        if (callbackBlock != nil) {
            callbackBlock();
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// initializePlayer:player - Setter method for the player property
// -------------------------------------------------------------------------------
- (void)initializePlayer:(id<RemoteMediaPlayer>)player {
    NSLog(@"Setting player to: %@", player);
    
    // Connect block
    void (^connectBlock)(void) = ^{
        if (player != nil) {
            // Set to the new player
            _player = player;
            // Register for callbacks, get current status
            [self connectToPlayer];
        }
    };

    [self resetUI];

    // Disconnect from earlier player if needed
    if (_player != nil) {
        // Unregister from callback
        [self disconnectFromPlayer:connectBlock];
    } else {
        connectBlock();
    }
}

#pragma mark - UI and Error Handling

// -------------------------------------------------------------------------------
// resetUI
// -------------------------------------------------------------------------------
- (void)resetUI {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.statusLabel.text = @"";
        self.positionSlider.value = 0.0;
        self.positionSlider.enabled = NO;
        self.currentPosition.text = @"";
        self.duration.text = @"";
    });
}

// -------------------------------------------------------------------------------
// synchronizeUI
// Update the UI when launching this view
// -------------------------------------------------------------------------------
- (void)synchronizeUI {
    [[self.player getDuration] continueWithBlock:^id(BFTask *durationTask) {
        if ([self handleError:durationTask.error]) {
            NSLog(@"Error while getting duration");
            return nil;
        } else {
            long long duration = [durationTask.result longLongValue];
            self.durationOfClip = duration;
            return [[self.player getStatus] continueWithBlock:^id(BFTask *statusTask) {
                if ([self handleError:statusTask.error]) {
                    NSLog(@"Error while getting status");
                    return nil;
                } else {
                    return [[self.player getPosition] continueWithBlock:^id(BFTask *positionTask) {
                        if ([self handleError:statusTask.error]) {
                            NSLog(@"Error while getting position");
                            return nil;
                        }
                        dispatch_async(self.statusUpdateQueue, ^{
                            MediaPlayerStatus* status = (MediaPlayerStatus*)(statusTask.result);
                            NSString* statusString = [self getDescriptionFromStatus:status];
                            bool mute = [status isMuteSet] ? [status isMute] : NO;
                            [self updatePlayerUI:status.state :statusString :[positionTask.result longLongValue] :duration :mute];
                        });
                        return nil;
                    }];
                }
            }];
        }
    }];
}

// -------------------------------------------------------------------------------
// updatePlayerUI:statusDescription:position:duration
// -------------------------------------------------------------------------------
- (void)updatePlayerUI : (enum MediaState) state : (NSString*) statusDescription : (long long) position : (long long) duration : (BOOL) mute {
    NSLog(@"updatePlayerUI: %@", statusDescription);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.statusLabel.text = statusDescription;
        [self.muteControl setImage: (mute ? [UIImage imageNamed:@"MuteOff"] : [UIImage imageNamed:@"MuteOn"]) forState:UIControlStateNormal];
        if (duration == 0 ||
            (state != Playing &&
             state != Paused &&
             state != Seeking)) {
            self.positionSlider.value = 0;
            self.positionSlider.enabled = NO;
            self.currentPosition.text = @"";
            self.duration.text = @"";
        } else {
            self.positionSlider.enabled = YES;
            self.positionSlider.value = (float)position/(float)duration;
            self.currentPosition.text = [self convertFromTime:position];
            self.duration.text = [self convertFromTime:duration];
        }
    });
}

// -------------------------------------------------------------------------------
// convertFromTime:time
// -------------------------------------------------------------------------------

#define MILLI_PER_SEC 1000
#define SEC_PER_MIN   60
#define MIN_PER_HOUR  60

- (NSString*) convertFromTime : (long long)time {
    // Create string out of parts
    int s = (time % (MILLI_PER_SEC * SEC_PER_MIN)) / MILLI_PER_SEC;
    int m = (time / (MILLI_PER_SEC * SEC_PER_MIN)) % MIN_PER_HOUR;
    long long h = (time / (MILLI_PER_SEC * SEC_PER_MIN * MIN_PER_HOUR));
    return [NSString stringWithFormat:@"%02lld:%02d:%02d", h, m, s];
}

// -------------------------------------------------------------------------------
//	handleError:error
//  Reports any error with an alert which was received from connection failures.
// -------------------------------------------------------------------------------
- (BOOL)handleError:(NSError *)error {
    @synchronized(self) {
        if (error == nil) {
            // Reset error count
            self.errorCount = 0;
            NSLog(@"Resetting the error count");
            return NO;
        }
        
        // Increment error count
        self.errorCount++;
        NSLog(@"Incremented error count to %ld", self.errorCount);
        
        // If error count > MAX, show error and exit
        if (self.errorCount > MAX_ERRORS) {
            NSString *errorMessage = [error localizedDescription];
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Communication Error"
                                                                           message:errorMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      if (self.observer != nil) {
                                                                          [self.observer connectionFailure];
                                                                      }
                                                                      [self resetUI];
                                                                  }];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    return YES;
}

#pragma mark - Interface to start flinging

// -------------------------------------------------------------------------------
// setMediaSource:url:metaData
// -------------------------------------------------------------------------------
- (void)setMediaSource: (NSString*)url : (NSString*)metaData {
    if (self.player) {
        long long interval = 1000;
        [[[self.player setPositionUpdateInterval:interval] continueWithBlock:^id(BFTask *task) {
            if ([self handleError:task.error]) {
                return nil;
            }
            return [self.player setMediaSourceToURL:url metaData:metaData autoPlay:YES andPlayInBackground:NO];
        }] continueWithBlock:^id(BFTask *task) {
            if ([self handleError:task.error]) {
                NSLog(@"Error in setMediaSource");
            }
            return nil;
        }];
    }
}

#pragma mark - UI Actions

// -------------------------------------------------------------------------------
// backward
// -------------------------------------------------------------------------------
- (IBAction)backward:(id)sender {
    [[self.player seekToPosition:-10000 andMode:RELATIVE] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Failed to seek.  Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// forward
// -------------------------------------------------------------------------------
- (IBAction)forward:(id)sender {
    [[self.player seekToPosition:10000 andMode:RELATIVE] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Failed to seek.  Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// play
// -------------------------------------------------------------------------------
- (IBAction)play:(id)sender {
    __block BOOL playCalled = NO;
    [[[self.player getStatus] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Error getting status, while trying to play");
            return nil;
        }
        MediaPlayerStatus* status = (MediaPlayerStatus*)task.result;
        if (status && [status state] == Paused) {
            NSLog(@"Status is Paused - Starting Playback");
            playCalled = YES;
            return [self.player play];
        } else if (status && [status state] == ReadyToPlay){
            NSLog(@"Status is ReadyToPlay - Starting Playback");
            playCalled = YES;
            return [self.player play];
        } else {
            NSLog(@"Incorrect state");
            return nil;
        }
    }] continueWithBlock:^id(BFTask *task) {
        if (playCalled && [self handleError:task.error]) {
            NSLog(@"Failed to play. Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// pause
// -------------------------------------------------------------------------------
- (IBAction)pause:(id)sender {
    __block BOOL pauseCalled = NO;
    [[[self.player getStatus] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Error getting status whie trying to pause");
            return nil;
        }
        MediaPlayerStatus* status = (MediaPlayerStatus*)task.result;
        if (status && [status state] == Playing) {
            NSLog(@"Status is Playing - Pausing Playback");
            pauseCalled = YES;
            return [self.player pause];
        } else {
            NSLog(@"Incorrect state");
            return nil;
        }
    }] continueWithBlock:^id(BFTask *task) {
        if (pauseCalled && [self handleError:task.error]) {
            NSLog(@"Failed to pause. Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// stop
// -------------------------------------------------------------------------------
- (IBAction)stop:(id)sender {
    [[self.player stop] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Failed to stop. Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// mute
// Mute functionality is unsupported on FireTV built-int receiver.
// However, it may be available in your app. The mute button is hidden on the UI, but the code is available for
// reference.
// -------------------------------------------------------------------------------
- (IBAction)mute:(id)sender {
    [[[self.player isMute] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Error while getting mute status");
            return nil;
        }
        if (task.result) {
            NSLog(@"Mute is %@", [task.result boolValue] ? @"on" : @"off");
            return [self.player setMute:![task.result boolValue]];
        } else {
            return nil;
        }
    }] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Failed to setMute. Error:%@", task.error);
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// userChangedPosition
// -------------------------------------------------------------------------------
- (IBAction)userChangedPosition:(id)sender {
    if (self.durationOfClip > 0) {
        long long newPosition = (long long)(self.positionSlider.value * self.durationOfClip);
        [[self.player seekToPosition:newPosition andMode:ABSOLUTE] continueWithBlock:^id(BFTask *task) {
            if ([self handleError:task.error]) {
                NSLog(@"Failed to seek.  Error:%@", task.error);
            }
            return nil;
        }];
    }
}

#pragma mark - Status Updates from Player

// -------------------------------------------------------------------------------
// onStatusChange:status:position
// -------------------------------------------------------------------------------
- (void) onStatusChange : (MediaPlayerStatus*) status positionChangedTo:(long long)position {
    // Need to get duration - in case the clip has changed
    [[self.player getDuration] continueWithBlock:^id(BFTask *task) {
        if ([self handleError:task.error]) {
            NSLog(@"Error getting duration");
        } else {
            dispatch_async(self.statusUpdateQueue, ^{
                NSString* statusString = [self getDescriptionFromStatus:status];
                bool mute = [status isMuteSet] ? [status isMute] : NO;
                long long duration = [task.result longLongValue];
                self.durationOfClip = duration;
                [self updatePlayerUI:status.state :statusString :position :duration :mute];
            });
        }
        return nil;
    }];
}

// -------------------------------------------------------------------------------
// getDescriptionFromStatus:status
// This method blocks while it retrieves the media info. Do not call this from
// main thread.
// -------------------------------------------------------------------------------
- (NSString*)getDescriptionFromStatus:(MediaPlayerStatus*)status {
    __block NSString* description = @"";
    switch (status.state) {
        case NoMedia:
            description = @"No Media";
            break;
        case PreparingMedia:
            description = @"Preparing Media";
            break;
        case ReadyToPlay:
            description = @"ReadyToPlay";
            break;
        case Playing:
            {
                // Need to get the current media info - in case the clip has changed by
                // another controller
                [[[self.player getMediaInfo] continueWithBlock:^id(BFTask *task) {
                    if ([self handleError:task.error]) {
                        NSLog(@"Failed to get MediaInfo.  Error:%@", task.error);
                    } else {
                        // Deserialize the string
                        NSError *error;
                        NSData *jsonData = [[task.result metadata] dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *properties = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
                        description = @"Playing ";
                        if (properties != nil) {
                            // String was correctly formatted, extract the fields
                            NSString* title = [properties objectForKey: @"title"];
                            description = [description stringByAppendingString: title];
                        } else {
                            description = [description stringByAppendingString:@"Unknown media"];
                        }
                    }
                    return nil;
                }] waitUntilFinished];
            }
            break;
        case Paused:
            description = @"Paused";
            break;
        case Seeking:
            description = @"Seeking";
            break;
        case Finished:
            description = @"Finished";
            break;
        case Error:
            description = @"Error";
            break;
        default:
            description = @"Unknown";
    }
    return description;
}

@end
