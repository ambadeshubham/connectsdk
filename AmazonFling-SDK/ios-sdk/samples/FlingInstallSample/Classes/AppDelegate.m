/**
 * AppDelegate.m
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import "AppDelegate.h"
#import "RootViewController.h"
#import "MediaSourceManager.h"

@interface AppDelegate ()

@end

#pragma mark -

@implementation AppDelegate

// -------------------------------------------------------------------------------
//	application:didFinishLaunchingWithOptions:
//  Initialize the main view and its subviews.
// -------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    MediaSourceManager* dbManager = [[MediaSourceManager alloc] init];
    RootViewController *rootViewController = (RootViewController*)[(UINavigationController*)self.window.rootViewController topViewController];
    
    rootViewController.mediaList = [[MediaListViewController alloc] initWithNibName:@"MediaListViewController" bundle:nil];
    [rootViewController.mediaList.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [rootViewController.view addSubview:rootViewController.mediaList.view];
    
    rootViewController.playerController = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
    [rootViewController.view addSubview:rootViewController.playerController.view];
    rootViewController.playerController.view.hidden = YES;
    
    rootViewController.mediaList.entries = [dbManager getAllSources];
    [rootViewController.mediaList.tableView reloadData];
    
    rootViewController.mediaList.observer = rootViewController;
    rootViewController.playerController.observer = rootViewController;
    
    return YES;
}

// -------------------------------------------------------------------------------
//	application:applicationWillResignActives:
//  Send out notification
// -------------------------------------------------------------------------------
- (void)applicationWillResignActive:(UIApplication *)application {
    NSLog(@"$$applicationWillResignActive");
    [[NSNotificationCenter defaultCenter] postNotificationName: @"willResignActive"
                                                        object: nil
                                                      userInfo: nil];
}

// -------------------------------------------------------------------------------
//	application:applicationDidBecomeActive:
//  Send out notification
// -------------------------------------------------------------------------------
- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"$$applicationDidBecomeActive");
    [[NSNotificationCenter defaultCenter] postNotificationName: @"willEnterForegound"
                                                        object: nil
                                                      userInfo: nil];
}

@end