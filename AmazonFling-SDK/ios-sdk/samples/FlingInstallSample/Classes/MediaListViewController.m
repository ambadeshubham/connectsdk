/**
 * MediaListViewController.m
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import "MediaListViewController.h"
#import "IconDownloader.h"
#import "MediaSourceManager.h"

#define kCustomRowCount 7

static NSString *CellIdentifier = @"MediaSourceTableCell";
static NSString *PlaceholderIdentifier = @"Placeholder";

@interface MediaSourceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbNail;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextView *details;

@end

@implementation MediaSourceTableViewCell

@end

@interface MediaListViewController () <UIScrollViewDelegate>

// the set of IconDownloader objects for each app
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

@end

@implementation MediaListViewController

// -------------------------------------------------------------------------------
//	viewDidLoad
// -------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[MediaSourceTableViewCell class] forCellReuseIdentifier:PlaceholderIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"MediaListRowCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
}

// -------------------------------------------------------------------------------
//	terminateAllDownloads
// -------------------------------------------------------------------------------
- (void)terminateAllDownloads {
    // terminate all pending download connections
    NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
    
    [self.imageDownloadsInProgress removeAllObjects];
}

// -------------------------------------------------------------------------------
//	dealloc
//  If this view controller is going away, we need to cancel all outstanding downloads.
// -------------------------------------------------------------------------------
- (void)dealloc {
    // terminate all pending download connections
    [self terminateAllDownloads];
}

// -------------------------------------------------------------------------------
//	didReceiveMemoryWarning
// -------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // terminate all pending download connections
    [self terminateAllDownloads];
}

#pragma mark - UITableViewDelegate

// -------------------------------------------------------------------------------
//	tableView: didSelectRowAtIndexPath:indexPath
// -------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MediaSource* source = [self.entries objectAtIndex:indexPath.row];
    if (self.observer != nil) {
        [self.observer mediaSourceSelected:source];
    }
}

#pragma mark - UITableViewDataSource

// -------------------------------------------------------------------------------
//	tableView:numberOfRowsInSection:
//  Customize the number of rows in the table view.
// -------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = self.entries.count;
    
    // if there's no data yet, return enough rows to fill the screen
    if (count == 0) {
        return kCustomRowCount;
    }
    return count;
}

// -------------------------------------------------------------------------------
//	tableView:cellForRowAtIndexPath:
// -------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MediaSourceTableViewCell *cell = nil;
    
    NSUInteger nodeCount = self.entries.count;
    
    if (nodeCount == 0 && indexPath.row == 0) {
        // add a placeholder cell while waiting on table data
        cell = [tableView dequeueReusableCellWithIdentifier:PlaceholderIdentifier forIndexPath:indexPath];
        cell.textLabel.text = @"Loading...";
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Leave cells empty if there's no data yet
        if (nodeCount > 0) {
            // Set up the cell representing the app
            MediaSource *mediaSource = (self.entries)[indexPath.row];
            
            cell.title.text = mediaSource.title;
            cell.details.text = [mediaSource.metadata objectForKey:@"description"];
            [cell.details setUserInteractionEnabled:NO];
            
            // Only load cached images; defer new downloads until scrolling ends
            if (!mediaSource.icon) {
                if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
                    [self startIconDownload:mediaSource forIndexPath:indexPath];
                }
                // if a download is deferred or in progress, return a placeholder image
                [cell.thumbNail setImage:[UIImage imageNamed:@"Placeholder.png"]];
            } else {
                [cell.thumbNail setImage:mediaSource.icon];
            }
            cell.thumbNail.contentMode = UIViewContentModeScaleAspectFit;
        }
    }

    return cell;
}

#pragma mark - Table cell image support

// -------------------------------------------------------------------------------
//	startIconDownload:forIndexPath:
// -------------------------------------------------------------------------------
- (void)startIconDownload:(MediaSource *)mediaSource forIndexPath:(NSIndexPath *)indexPath {
    IconDownloader *iconDownloader = (self.imageDownloadsInProgress)[indexPath];
    if (iconDownloader == nil)  {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.mediaSource = mediaSource;
        [iconDownloader setCompletionHandler:^{
            
            MediaSourceTableViewCell *cell = (MediaSourceTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            // Display the newly loaded image
            cell.thumbNail.image = mediaSource.icon;
            
            // Remove the IconDownloader from the in progress list.
            // This will result in it being deallocated.
            [self.imageDownloadsInProgress removeObjectForKey:indexPath];
            
        }];
        (self.imageDownloadsInProgress)[indexPath] = iconDownloader;
        [iconDownloader startDownload];
    }
}

// -------------------------------------------------------------------------------
//	loadImagesForOnscreenRows
//  This method is used in case the user scrolled into a set of cells that don't
//  have their app icons yet.
// -------------------------------------------------------------------------------
- (void)loadImagesForOnscreenRows {
    if (self.entries.count > 0) {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths) {
            MediaSource *mediaSource = (self.entries)[indexPath.row];
            
            if (!mediaSource.icon) {
                // Avoid the app icon download if the app already has an icon
                [self startIconDownload:mediaSource forIndexPath:indexPath];
            }
        }
    }
}

#pragma mark - UIScrollViewDelegate

// -------------------------------------------------------------------------------
//	scrollViewDidEndDragging:willDecelerate:
//  Load images for all onscreen rows when scrolling is finished.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadImagesForOnscreenRows];
    }
}

// -------------------------------------------------------------------------------
//	scrollViewDidEndDecelerating:scrollView
//  When scrolling stops, proceed to load the app icons that are on screen.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadImagesForOnscreenRows];
}

@end
