/**
 * RootViewController.h
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import <UIKit/UIKit.h>
#import <AmazonFling/DiscoveryController.h>
#import <AmazonFling/RemoteMediaPlayer.h>

#import "MediaListViewController.h"
#import "PlayerViewController.h"
#import "MediaListObserver.h"
#import "ConnectionObserver.h"

@interface RootViewController : UIViewController <MediaListObserver,
                                                 ConnectionObserver,
                                                  DiscoveryListener,
                                              UIActionSheetDelegate>

// The media list that embeds UITableView - owned
@property (nonatomic, strong) MediaListViewController* mediaList;

// This view controller is displayed when we connect to the remote player - owned
@property (nonatomic, strong) PlayerViewController* playerController;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *picker;

- (IBAction)onPickerClicked:(id)sender;

// Protocol implementation

// MediaListObserver
- (void)mediaSourceSelected:(MediaSource*) source;

// ConnectionObserver
- (void)connectionFailure;

// DiscoveryController
- (void)deviceDiscovered:(id<RemoteMediaPlayer>)device;
- (void)deviceLost:(id<RemoteMediaPlayer>)device;
- (void)discoveryFailure;

@end