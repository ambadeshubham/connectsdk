/**
 * MediaListViewController.h
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import <UIKit/UIKit.h>
#import "MediaListObserver.h"

@interface MediaListViewController : UITableViewController

// the main data model for our UITableView
@property (nonatomic, strong) NSArray *entries;

// the observer to be notified when an item is selected - not owned
@property (nonatomic, weak) id<MediaListObserver> observer;

@end
