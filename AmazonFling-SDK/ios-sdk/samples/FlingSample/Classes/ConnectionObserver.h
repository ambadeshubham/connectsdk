/**
 * ConnectionObserver.h
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import <Foundation/Foundation.h>

@protocol ConnectionObserver <NSObject>

// This method is called on the observers when the connection is successful.
- (void)connected;

// This method is called on the observers when the connection is terminated deliberately.
- (void)disconnected;

// This method is called on the observers when the connection breaks.
- (void)connectionFailure;

@end
