/**
 * PlayerViewController.h
 *
 * Copyright (c) 2015 Amazon Technologies, Inc. All rights reserved.
 *
 * PROPRIETARY/CONFIDENTIAL
 *
 * Use is subject to license terms.
 */

#import <UIKit/UIKit.h>
#import <AmazonFling/RemoteMediaPlayer.h>
#import <AmazonFling/MediaPlayerStatus.h>
#import <AmazonFling/MediaPlayerInfo.h>
#import <Bolts/BFTask.h>

#import "ConnectionObserver.h"

@interface PlayerViewController : UIViewController <MediaPlayerStatusListener>

// The remote media player instance - not owned
@property (nonatomic, weak, setter=setPlayer:) id<RemoteMediaPlayer> player;

// The observer to be notified when the connection fails - not owned
@property (nonatomic, weak) id<ConnectionObserver> observer;

// The interface to start playback on the remote player
- (void)setMediaSource: (NSString*)url : (NSString*)metaData;

@property (weak, nonatomic) IBOutlet UISlider *positionSlider;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentPosition;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIButton *muteControl;

- (IBAction)backward:(id)sender;
- (IBAction)foreward:(id)sender;
- (IBAction)play:(id)sender;
- (IBAction)pause:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)mute:(id)sender;
- (IBAction)userChangedPosition:(id)sender;

// Protocol implementation
- (void)onStatusChange: (MediaPlayerStatus*) status positionChangedTo:(long long)position;

@end
